<?php

/**
 * Core
 */
require get_template_directory() . '/inc/functions/core.php';

/**
 * Scripts
 */
require get_template_directory() . '/inc/functions/scripts.php';

// /**
//  * Menus
//  */
require get_template_directory() . '/inc/functions/menus.php';

// /**
//  * Hiding editor
//  */
require get_template_directory() . '/inc/functions/hiding-editor.php';

// /**
//  * ACF
//  */
require get_template_directory() . '/inc/functions/acf.php';

// /**
//  * Custom image sizes
//  */
require get_template_directory() . '/inc/functions/custom-image-sizes.php';

// /**
//  * Excerpt
//  */

require get_template_directory() . '/inc/functions/excerpt.php';

