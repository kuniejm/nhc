<?php get_template_part('head'); ?>
<?php 
	$contact = get_field('contact', 'options');
	$bg = get_field('bg');
?>
<header data-scroll-section<?php if ($bg): ?> style="background-color:  <?= $bg; ?>"<?php endif; ?>>
	<div class="container-fluid pe-lg-0 h-100">
		<div class="row me-lg-0 h-100 align-items-center">
			<div class="col-6 col-lg-4 col-xl-3 offset-xl-2">
				<a href="<?= home_url('/'); ?>" class="logo d-inline-block js-custom-exit">
					<img src="<?= get_template_directory_uri(); ?>/img/static/logo.svg" alt="NHC - Nordic Hydrogen Corridor">
				</a>	
			</div>	
			<div class="col-6 col-lg-4 text-end text-lg-start">
				<div class="d-inline-block">
					<img src="<?= get_template_directory_uri(); ?>/img/static/eu.svg" alt="Funded by the European Union" class="logo-ue">
				</div>	
			</div>	
			<?php if ($contact): ?>		
				<div class="col-auto pe-0 ms-auto header-contact h-100 d-none d-lg-block">
					<a href="<?= get_permalink($contact); ?>" title="<?= get_the_title($contact); ?>" class="fs-14 hover-skew px-7 d-inline-flex align-items-center h-100 pt-1 text-uppercase js-custom-exit">
						<span class="position-relative zIndex2">
							<?= get_the_title($contact); ?>
						</span>
					</a>						
				</div>
			<?php endif; ?>
		</div>
	</div>
</header>
<a href="<?= home_url('/'); ?>" title="Menu" class="hamburger hover-skew d-flex justify-content-center align-items-center<?php if (is_front_page()): ?> is-transparent<?php endif; ?>">
	<span class="hamburger-inner d-inline-block">
		<span class="line"></span>
		<span class="line"></span>
		<span class="line"></span>			  			
	</span>
</a>
<?php get_template_part('inc/nav'); ?>