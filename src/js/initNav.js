import { gsap } from 'gsap';
import { showUp, isMobile } from './utils';
import { scroll } from './initScroll';
import Overlay from './initOverlay';

export const nav = {
	initEvents: () => {
    nav.hamburger.addEventListener('click', (e) => {
    	e.preventDefault();
      nav.hamburger.classList.add('is-disabled');
      nav.inner = document.querySelector('.nav-main .container-fluid');
    	nav.hamburger.classList.toggle('is-active');
      if (nav.hamburger.classList.contains('is-active')) {
        nav.showMenu(); 
        nav.inner.focus(); 
      } else {
        nav.hideMenu();
        nav.hamburger.focus();
      }
    });

    if (nav.submenus !== null) {
      nav.submenus.forEach(item => {
        item.addEventListener('click', (e) => {
          e.preventDefault();
          nav.toggleSubmenu(item);
        })
      });
    }
	},

  toggleSubmenu: (item) => {
    let ul = item.nextElementSibling;
    let count = ul.querySelectorAll('li').length;
    let allTogglers = document.querySelectorAll('.toggle-submenu.is-active');

    if (allTogglers !== null) {
      allTogglers.forEach(el => {
        if (el !== item) {
          let sub = el.nextElementSibling;
          el.classList.remove('is-active');
          sub.classList.remove('is-active');
          gsap.to(sub, .3, { height: 0});          
        }
      });      
    }

    if (item.classList.contains('is-active')) {
      gsap.to(ul, .3, { height: 0});
    } else {
      gsap.to(ul, .5, { height: count * 75});
    }
    
    item.classList.toggle('is-active');
    ul.classList.toggle('is-active');
  },

	showMenu: () => {
    scroll.scrollStop();
    nav.overlay.showOverlay();
    gsap.to(nav.content, .5, { opacity: 1, delay: 1, display: 'block', onComplete: function() {
      nav.body.classList.add('nav-open');
    }})
	},

	hideMenu: () => {
    nav.overlay.hideOverlay();
    scroll.scrollStart();
    gsap.to(nav.content, .5, { opacity: 0, delay: .2, display: 'none', onComplete: function() {
      nav.body.classList.remove('nav-open');
    }})
	},

	init: () => {
   nav.body = document.body;
   nav.overlay = new Overlay('.overlay-menu');
   nav.hamburger = document.querySelector('.hamburger');
   nav.content = document.querySelector('.nav-main');
   nav.submenus = document.querySelectorAll('.menu-item-has-children .toggle-submenu');

   nav.initEvents();
	}
}