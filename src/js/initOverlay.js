import { gsap } from 'gsap';
import { CustomEase } from './CustomEase';
import { scroll } from './initScroll';
gsap.registerPlugin(CustomEase);

const body = document.querySelector('body');
const ease = CustomEase.create('bezier', '0.84, 0, 0.16, 1');

const hideOverlay = (overlay, firstTime) => {
  let overlayInner = overlay.querySelectorAll('span:not(.exclude)');
  let j = overlayInner.length - 1;
  setTimeout(() => {
    overlayInner.forEach((span, i) => {
      gsap.fromTo(span, {scaleX: 1, transformOrigin: "0 50%"}, {scaleX: 0, transformOrigin: "100% 50%", delay: (i/8), duration: .4, ease: ease, onComplete: function() {
        if (i == j) {
          gsap.set(overlay, {autoAlpha: 0, display:'none'});
          body.classList.remove('overlay-open');     
          document.querySelector('.hamburger').classList.remove('is-disabled');

          if (firstTime) {
            scroll.init();
          }     
        } 
      }}); 
    }) 
  }, 50)
}

export default class Overlay {
  constructor(el) {
    this.overlay = document.querySelector(el);
    this.link = undefined;
    this.ease = ease
  }

  showOverlay() {
    let overlay = this.overlay;
    let overlayInner = overlay.querySelectorAll('span:not(.exclude)');
    let newLink = document.querySelector('input[name="custom-exit-url"]');
    let j = overlayInner.length - 1;

    gsap.to(overlay, 0.1, {autoAlpha: 1, display:'flex'});
    overlayInner.forEach((span, i) => {
      gsap.fromTo(span, {scaleX: 0, x: 0, transformOrigin: "0 50%"}, {x: 0, scaleX: 1, transformOrigin: "0 50%", delay: (i/10), duration: .6, ease: ease, onComplete: function() {
        if (i === 0) {
          body.classList.remove('overlay-open');
          document.querySelector('.hamburger').classList.remove('is-disabled');
          if (newLink !== undefined && newLink !== '#' && newLink !== null && !document.body.classList.contains('is-swiping')) {
            window.location.href = newLink.getAttribute('value');
          }          
        }
      }}); 
    }) 
  }

  hideOverlay(firstTime) {
    let overlay = this.overlay;
    hideOverlay(overlay, firstTime); 
  }
}

