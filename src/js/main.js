require('intersection-observer');
//import 'bootstrap/js/dist/collapse';
import LocomotiveScroll from 'locomotive-scroll';
import Slider from './initSlider';
import { matchHeight, isMobile } from './utils';
import { video } from './video';
import { words } from './animations';
import { transition } from './pageTransition';
import { nav } from './initNav';
import { lightbox } from './initLightbox';
import { map } from './initMap';
import { timeline } from './initTimeline';

window.addEventListener('load', function() {
  video.init();
  matchHeight();
  words.init();
  transition.init();
  nav.init();
  lightbox.init();
  new Slider('.slider-more');
  new Slider('.slider-nav');
  new Slider('.slider-img');
  new Slider('.slider-text');
  new Slider('.slider-map');
  new Slider('.slider-circles');
  map.init('.map0');
  map.init('.map1');
  timeline.init();
});

document.addEventListener("DOMContentLoaded", () => {
  matchHeight();
});
