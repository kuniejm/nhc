import Slider from './initSlider';

export const timeline = {
	init: () => {
   timeline.body = document.body;
   timeline.pagination = document.querySelector('.slider-phases');
   timeline.slides = document.querySelector('.slider-timeline');

   if (timeline.pagination !== null) {
     new Slider('.slider-timeline');
     new Slider('.slider-phases');    

     timeline.pagination.swiper.on('slideChangeTransitionEnd', () => {
      let index_currentSlide = timeline.pagination.swiper.realIndex;
      let currentSlide = timeline.pagination.swiper.slides[index_currentSlide]
      timeline.slides.swiper.slideTo(index_currentSlide, 200, false);
    });

     timeline.slides.swiper.on('slideChangeTransitionEnd', () => {
      let index_currentSlide = timeline.slides.swiper.realIndex+1;
      let currentSlide = timeline.slides.swiper.slides[index_currentSlide]
      timeline.pagination.swiper.slideTo(index_currentSlide, 200, false);
    });

   }
 }
}