import Swiper, { Navigation, Lazy, Pagination, EffectFade } from 'swiper';
Swiper.use([Lazy, Navigation, Pagination, EffectFade]);

export default class Slider {
  constructor(el) {
    this.selector = el;
    this.slider = document.querySelectorAll(el);
    if (this.slider !== null) {
      this.slider.forEach(el => {
        this.initSlider(el);
      })

    }
  }

  initSlider(el) {
    let speed = 4000;
    let index = 0;
    let space = el.getAttribute('data-space') ? parseInt(el.getAttribute('data-space')) : 1;
    let perView = el.getAttribute('data-perview') ? parseInt(el.getAttribute('data-perview')) : 'auto';
    let loop = el.getAttribute('data-loop') ? parseInt(el.getAttribute('data-loop')) : 1;
    let pagination = el.getAttribute('data-pagination') ? 1 : 0;
    let fade = el.getAttribute('data-fade') ? 1 : 0;
    let offsetAfter = el.getAttribute('data-offset') ? window.innerWidth * .25 : 0;
    let start = el.getAttribute('data-start') ? parseInt(el.getAttribute('data-start')) : 0;

    let parent = el.parentElement;
    let grandpa = parent.parentElement;
    let arrowFinder = el;
    let spaceDesktop = space;
    if (grandpa !== null) {
      arrowFinder = grandpa
    }
    if (space > 20) {
      spaceDesktop = space / 2;
    }
    let options = {
      slidesPerView: perView,
      spaceBetween: spaceDesktop,
      initialSlide: index,
      init: false,
      slideToClickedSlide: true,
      watchSlidesVisibility: true,
      loop: loop,
      slidesOffsetAfter: 0,
      lazy: true,
      observer: true,
      observeParents: true,
      grabCursor: true,  
      navigation: {
        nextEl: arrowFinder.querySelector('.swiper-button-next'),
        prevEl: arrowFinder.querySelector('.swiper-button-prev'),
      }, 
      breakpoints: {
        575: {
          spaceBetween: space
        },
        992: {
          slidesOffsetAfter: offsetAfter,
        }
      },
      on: {
        sliderMove: function() {
          document.body.classList.add('is-swiping')
        },

        transitionEnd: function() {
          setTimeout(() => {
            document.body.classList.remove('is-swiping')
          }, 900)
        },
      }
    }

    if (start) {
      options.initialSlide = start;
    }

    if (pagination) {
      options.pagination = {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: true
      }
    }

    if (fade) {
      options.effect = 'fade';
      options.fadeEffect = {
        crossFade: true
      }
    }

    let swiper = new Swiper(this.selector, options);
    swiper.init();

    setTimeout(() => {
      swiper.update();
    }, 200);
  }
}
