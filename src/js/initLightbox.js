import { gsap, Expo } from 'gsap';
import { isMobile } from './utils';
import { scroll } from './initScroll';
import Overlay from './initOverlay';

export const lightbox = {
	initEvents: () => {
    lightbox.opener.forEach(opener => {
      opener.addEventListener('click', (e) => {
        e.preventDefault();
        let id = opener.getAttribute('data-id');
        lightbox.showLightbox(id);
      });
    });

    lightbox.close.addEventListener('click', (e) => {
      e.preventDefault();
      lightbox.hideLightbox();
    });
	},

	showLightbox: (id) => {
    lightbox.content = document.querySelector('.lightbox[data-id="' + id + '"]');
    scroll.scrollStop();
    lightbox.overlay.showOverlay();
    lightbox.content.classList.add('is-active');
    gsap.to(lightbox.content, 1, { opacity: 1, display: 'block', onComplete: function() {
      lightbox.body.classList.add('lightbox-open');
    }})

	},

	hideLightbox: () => {
    if (lightbox.content)
    lightbox.body.classList.remove('lightbox-open');
    lightbox.overlay.hideOverlay();
    scroll.scrollStart();
    gsap.to(lightbox.content, .4, { opacity: 0, display: 'none' });
	},

	init: () => {
   lightbox.body = document.body;
   lightbox.content;
   lightbox.opener = document.querySelectorAll('.js-lightbox');
   lightbox.close = document.querySelector('.js-lightbox-close');
   lightbox.overlay = new Overlay('.overlay-lightbox');
   if (lightbox.opener !== null) {
     lightbox.initEvents();
   }
	}
}