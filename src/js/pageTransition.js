import Overlay from './initOverlay';
import { clickLinks } from './utils';

export const transition = {
	initEvents: () => {
		//safari fix for back button
		window.onpageshow = function(event) {
			if (event.persisted) {
				window.location.reload();
			}
		};
	},

	init: () => {
		transition.initEvents();
		const overlay = new Overlay('.overlay');
		let firstTime = 1;
		overlay.hideOverlay(firstTime);

		//const links = document.querySelectorAll('a.js-custom-exit');
		//clickLinks(links, overlay);
	}
}

