import LocomotiveScroll from 'locomotive-scroll';
import LazyLoad from "vanilla-lazyload";

export const scroll = {
	init: () => {
		scroll.scroller = new LocomotiveScroll({
			el: document.querySelector('[data-scroll-container]'),
			smooth: true
		});

		scroll.initLazyLoad();

		if (document.querySelector('.home') !== null) {
			scroll.scroller.on('scroll', ({ limit, scroll }) => {

			  if (scroll.y > 100) {
			  	document.querySelector('.hamburger').classList.remove('is-transparent');
			  	document.querySelector('.nav-panel').classList.remove('is-transparent');
			  } else {
			  	document.querySelector('.hamburger').classList.add('is-transparent');
			  	document.querySelector('.nav-panel').classList.add('is-transparent');
			  }
			})
		}

		if (document.querySelector('.section-nav') !== null && window.innerWidth >= 992) {
			scroll.scroller.on('scroll', ({ limit, scroll }) => {
			  const progress = scroll.y;
			  const headerH = document.querySelector('header').clientHeight;
			  let translate = (progress <= headerH) ? progress : headerH;
			  if (document.querySelector('.section-nav') !== null) {
				  if (progress <= 150) {
				  	document.querySelector('.section-nav').style.transform = `translate3d(0, -${translate}px, 0)`
				  }		  	
			  }
			})			
		}

		setTimeout(() => {
			scroll.scrollupdate();
		}, 500)

		setTimeout(() => {
			scroll.scrollupdate();
		}, 2500)

		// scroll.scroller.on('call', (obj) => {

		// });
	},

	initLazyLoad: () => {
	  const lf = new LazyLoad({
	    unobserve_entered: true,
	    elements_selector: 'iframe',
	    callback_loaded: () => {
	      scroll.scrollupdate()
	    },
	    callback_enter: () => {
	      scroll.scrollupdate()
	    }
	  });

	  const ll = new LazyLoad({
	    unobserve_entered: true,
	    elements_selector: '.lazyload',
	    callback_loaded: () => {
	      scroll.scrollupdate()
	    },
	    callback_enter: () => {
	      scroll.scrollupdate()
	    }
	  });
	},

	scrollStop: () => {
		scroll.scroller.stop();
	},

	scrollStart: () => {
		scroll.scroller.start();
	},

	scrollupdate: () => {
		scroll.scroller.update();
	},

	scrollToEl: (el) => {
		scroll.scroller.scrollTo(el);
	},
}