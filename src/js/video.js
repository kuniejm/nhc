import Plyr from 'plyr';


export const video = {
	init: () => {
		video.player = document.querySelectorAll('.player');

		if (video.player !== null) {
			video.player.forEach(video => {
				let id = video.getAttribute('id');
				const player = new Plyr('#' + id, {
					controls: [],
					autoplay: true,
					muted: true,
					loop: { active: true },
					storage: { enabled: false }
				});
			});
		}
	}
}