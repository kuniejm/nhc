import { mapStyles } from './utils/mapStyles';
import { isMobile } from './utils';
import Slider from './initSlider';

export const map = {
	initSlider: () => {
		let slider = map.infoWindow.querySelector('.swiper-container').swiper;
    new Slider('.slider-map');

   	slider.on('transitionEnd', function() {
   		map.sliderOnChange(slider, true);
		});
	},

	setCustomControls: () => {
		map.zoomOut.addEventListener('click', (e) => {
			e.preventDefault();
			let currentZoomLevel = map.inner.getZoom();
			if (currentZoomLevel != 0){
				map.inner.setZoom(currentZoomLevel - 1);}     
		});

		map.zoom.addEventListener('click', (e) => {
			e.preventDefault();
			let currentZoomLevel = map.inner.getZoom();
			if (currentZoomLevel != 21){
				map.inner.setZoom(currentZoomLevel + 1);}     
		});
	},

	sliderOnChange: (slider, pan) => {
		let index = slider.realIndex;
		if (pan) {
			map.inner.panTo(map.markers[index].getPosition());
		}
		map.markers.forEach((marker, i) => {
   		marker.setIcon(map.markersPos[i].icon);
			marker.setZIndex(1);
		});

		map.markers[index].setIcon(map.markerCurrent);
		map.markers[index].setZIndex(5);			

	},

	createMarkers: () => {
		let parent = map.parent;
		let icon;
		if (parent !== null) {
			let slides = parent.querySelectorAll('[data-lat]:not(.swiper-slide-duplicate');
			slides.forEach(slide => {
				let lat = slide.getAttribute('data-lat');
				let lng = slide.getAttribute('data-lng');
				let cat = slide.getAttribute('data-cat');	

				if (cat == 'cat2') {
					icon = map.marker.replace('marker-blue.svg', 'marker-pink.svg');
				} else if (cat == 'cat1') {
					icon = map.marker.replace('marker-blue.svg', 'marker-light.svg');
				} else if (cat == 'cat3') {
					icon = map.marker.replace('marker-blue.svg', 'marker-yellow.svg');
				} else {
					icon = map.marker;
				}
				map.markersPos.push({lat, lng, cat, icon});
			});
		}
	},

	changeSlideIndex: (i) => {
		let slider = map.infoWindow.querySelector('.swiper-container').swiper;
		if (slider !== null) {
			slider.slideTo(i);
			slider.update();
		}
	},

	assignMarkers: () => {
		map.markersPos.forEach((marker, i) => {
			let cat = marker.cat;
			const mrkr = new google.maps.Marker({
				position: new google.maps.LatLng(marker.lat, marker.lng),
				icon: map.markersPos[i].icon,
				cat: cat,
				map: map.inner,
				zIndex: 1
			});

			map.markers.push(mrkr);

			google.maps.event.addListener(mrkr, 'click', () => {
				map.transition = 1;
   			map.changeSlideIndex(i);
   			if (!map.infoWindow.classList.contains('is-active')) {
   				map.infoWindow.classList.add('is-active');
   			}
   			map.infoWindow.classList.remove('is-inactive');
   			map.inner.panTo(mrkr.getPosition());
   			map.markers.forEach((marker, i) => {
   				marker.setIcon(map.markersPos[i].icon);
   				marker.setZIndex(1);
   			})
   			let current = '';
   			if (mrkr.getIcon().indexOf('-blue.svg') !== -1) {
   				current = mrkr.getIcon().replace('marker-blue.svg', 'marker-blue-current.svg')
   			} else if (mrkr.getIcon().indexOf('-light.svg') !== -1) {
					current = mrkr.getIcon().replace('marker-light.svg', 'marker-light-current.svg')
   			} else {
					current = mrkr.getIcon().replace('marker-yellow.svg', 'marker-yellow-current.svg')
   			}
   			mrkr.setIcon(current);
   			

   			mrkr.setZIndex(5);
   		});
		})

	},

	setMap: (selector) => {
		map.inner = new google.maps.Map(selector, {
			zoom: (isMobile && window.innerWidth <= 767) ? 4 : 6,
			mapTypeControl: false,
			streetViewControl: false,
			fullscreenControl: false,
			styles: map.styles,
			center: new google.maps.LatLng(map.centerLat, map.centerLng),
			mapTypeId: 'roadmap',
			disableDefaultUI: true,
			scrollwheel: false, //disable scroll to zoom
			clickableIcons: false, //disable point-of-interest infowindow
			options: {
		    gestureHandling: 'greedy' //disable cmd to zoom
		  }
		});
		
		map.assignMarkers();
	},

	initFilters: () => {
		if (map.filters !== null) {
			map.filters.forEach(filter => {
				filter.addEventListener('click', (e) => {
					e.preventDefault();
					map.filters.forEach(fltr => {
		        if (fltr !== filter) {
		          fltr.classList.remove('is-active');        
		        }
					})

	        filter.classList.toggle('is-active');
	        map.filterMarkers(filter.dataset.cat);
				})
			})
		}
	},

	filterMarkers: (cat) => {
		if (map.filters[0].parentElement.querySelector('.is-active') == null) {
			map.markers.forEach((marker) => {
				marker.setVisible(true);
			});				
		} else {
			map.markers.forEach((marker) => {
				if (marker.cat == cat) {
					marker.setVisible(true);
				} else {
					marker.setVisible(false);
				}
			});	
		}
	},

	init: (el) => {
		map.inner;
		map.wrapper = document.querySelector(el);
		if (map.wrapper !== null && typeof google !== "undefined") {
			map.parent = map.wrapper.parentElement;
			map.filters = map.parent.querySelectorAll('.map-filters a');
			map.infoWindow = map.parent.querySelector('.slider-map-container');
			map.styles = mapStyles.mapStyles;
			map.centerLat = parseFloat(map.wrapper.getAttribute('data-center-lat'));
			map.centerLng = parseFloat(map.wrapper.getAttribute('data-center-lng'));
			map.markerCurrent = map.wrapper.getAttribute('data-current');
			map.marker = map.wrapper.getAttribute('data-marker');
			map.zoom = map.parent.querySelector('.map-controls-zoom');
			map.zoomOut = map.parent.querySelector('.map-controls-zoomout');
			map.markersPos = [];
			map.markers = [];
			map.createMarkers();
			map.setMap(map.wrapper);
			map.setCustomControls();
			map.initFilters();
		  map.initSlider();
		}
	}
}