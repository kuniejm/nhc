<?php /* Template Name: Newses */ ?>

<?php get_header(); ?>

<section id="section0" class="section-text pb-0" data-scroll-section>
	<div class="container-fluid">
		<div class="row">
			<h1 class="col-xl-8 offset-xl-2 pb-100 mb-md-7" data-scroll data-splitting><?php the_title(); ?></h1>

			<h2 class="h2-medium col-xl-8 offset-xl-2" data-scroll data-splitting><?php _e('Latest news', 'nhc'); ?></h2>
		</div>
	</div>
	<div class="line-decorative" data-scroll></div>
</section>

<?php get_template_part('inc/sections/newses'); ?>
<?php get_template_part('inc/sections/publications'); ?>


<?php get_footer(); ?>