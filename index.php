<?php get_header(); ?>

<?php 

if (is_front_page()) {
	get_template_part('inc/home/hero');
}

if (is_singular('post') || is_singular('news')) {
	get_template_part('inc/sections/img_100', null, array('crop' => true)); 
	get_template_part('inc/single/meta'); 
} 
else {
	get_template_part('inc/sections/nav_children');
}
if (is_home()) {
	$id = get_option( 'page_for_posts' );
} else {
	$id = get_the_ID();
}
?>

<?php if( have_rows('sections', $id) ):
	$index = 0;
	while ( have_rows('sections', $id) ) : the_row(); 

		if( get_row_layout() == '100%_text' ):
			get_template_part('inc/sections/text_100', null, array('index' => $index));
		elseif ( get_row_layout() == '2_columns' ):
			get_template_part('inc/sections/columns', null, array('index' => $index));
		elseif ( get_row_layout() == '2_columns_title' ):
			get_template_part('inc/sections/columns_title', null, array('index' => $index));
		elseif ( get_row_layout() == '2_columns_title_img' ):
			get_template_part('inc/sections/columns_title_img', null, array('index' => $index));
		elseif ( get_row_layout() == 'image_caption' ):
			get_template_part('inc/sections/img_caption', null, array('index' => $index));
		elseif ( get_row_layout() == '100%_image' ):
			get_template_part('inc/sections/img_100', null, array('index' => $index));
		elseif ( get_row_layout() == 'quote' ):
			get_template_part('inc/sections/quote', null, array('index' => $index));
		elseif ( get_row_layout() == 'spacing' ):
			get_template_part('inc/sections/spacing', null, array('index' => $index));
		elseif ( get_row_layout() == 'more' ):
			get_template_part('inc/sections/more', null, array('index' => $index));
		elseif ( get_row_layout() == 'image_slider' ):
			get_template_part('inc/sections/image_slider', null, array('index' => $index));
		elseif ( get_row_layout() == 'lightbox_text' ):
			get_template_part('inc/sections/lightbox_text', null, array('index' => $index));
		elseif ( get_row_layout() == 'lightbox_youtube' ):
			get_template_part('inc/sections/youtube', null, array('index' => $index));
		elseif ( get_row_layout() == 'video' ):
			get_template_part('inc/sections/video', null, array('index' => $index));
		elseif ( get_row_layout() == 'numbered_slider' ):
			get_template_part('inc/sections/numbered_slider', null, array('index' => $index));
		elseif ( get_row_layout() == 'timeline' ):
			get_template_part('inc/sections/timeline', null, array('index' => $index));
		elseif ( get_row_layout() == 'sllider_circles' ):
			get_template_part('inc/sections/slider_circles', null, array('index' => $index));
		elseif ( get_row_layout() == 'icons' ):
			get_template_part('inc/sections/icons', null, array('index' => $index));
		elseif ( get_row_layout() == 'map_refuelling' ):
			get_template_part('inc/maps/map_refuelling', null, array(
				'index' => $index, 
				'lat' => get_sub_field('center', $id)['lat'] ? get_sub_field('center', $id)['lat'] : '59.1214002',
				'lng' => get_sub_field('center', $id)['lng'] ? get_sub_field('center', $id)['lng'] : '9.6358043'
			));
		elseif ( get_row_layout() == 'map_nordic' ):
			get_template_part('inc/maps/map_nordic', null, array(
				'index' => $index, 
				'lat' => get_sub_field('center', $id)['lat'] ? get_sub_field('center', $id)['lat'] : '59.1214002',
				'lng' => get_sub_field('center', $id)['lng'] ? get_sub_field('center', $id)['lng'] : '9.6358043'
			));

		endif; 
		$index++;
	endwhile;
endif;

if (is_home()) {
	get_template_part('inc/sections/blog');
}

if (is_page_template('template-partners.php')) {
	get_template_part('inc/sections/partners');
}

if (is_front_page() || is_singular('news')) {
	get_template_part('inc/home/news');
}
?>

<?php get_footer(); ?>
