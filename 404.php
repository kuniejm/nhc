<?php get_header(); ?>

<section data-scroll-section class="error404">
	<div class="container-fluid pt-100 pb-100">
		<div class="row justify-content-center pt-10 pb-10">
			<h1 class="col-12 text-center mb-10">404</h1>
			<div class="paragraph col-12 text-center hover-underline">
				<a href="<?= home_url('/'); ?>"><?php _e('Go back home', 'nhc'); ?></a>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>