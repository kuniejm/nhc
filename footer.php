<?php 
  $email = get_field('email', 'option');
?>  
  <footer data-scroll-section>
    <div class="container-fluid" data-scroll data-scroll-opacity>
      <div class="row">
        <div class="col-6 col-md-3 offset-xl-1 py-10 my-sm-5">
          <a href="<?= home_url('/'); ?>" class="logo js-custom-exit">
            <img src="<?= get_template_directory_uri(); ?>/img/static/logo.svg" height="36" width="282" alt="">
          </a>  
        </div>
        <div class="col-6 col-md-3 py-10 my-sm-5 logo-ue">
          <img src="<?= get_template_directory_uri(); ?>/img/static/eu.svg" height="36" width="150" alt="">
        </div>
      </div>
    </div>
    <div class="line-decorative" data-scroll data-scroll-target="footer"></div>

    <div class="container-fluid" data-scroll data-scroll-opacity>
      <div class="row">
        <div class="col-12 col-lg-5 py-10 py-sm-7 hover-underline offset-xl-1 d-flex flex-column flex-xl-row align-items-start align-items-xl-center justify-content-between">
          <?php if ($email): ?>
          <div class="fs-14 mb-2 mb-xl-0"><?php _e('E-mail', 'nhc'); ?></div>
          <a href="mailto:<?= $email; ?>" class="footer-email lh-1"><?= $email; ?></a>
          <?php endif; ?>
        </div>
        <div class="col-12 col-lg-5 ps-lg-10 py-10 py-sm-7 ms-lg-7 footer-follow d-flex flex-column flex-xl-row align-items-xl-center justify-content-between">
          <div class="fs-14 mb-2 mb-xl-0"><?php _e('Follow us', 'nhc'); ?></div>
          <?php get_template_part('inc/general/socials'); ?>
        </div>
      </div>
    </div>

    <div class="line-decorative" data-scroll data-scroll-target="footer"></div>

    <div class="container-fluid" data-scroll data-scroll-opacity> 
      <div class="row">
        <div class="col-md-8 offset-xl-1 pt-10 pb-5 mt-sm-5">
          <ul class="d-flex flex-wrap text-lowercase fs-14 hover-underline">
            <?php wp_nav_menu( array( 
              'theme_location' => 'footer_menu', 
              'menu_class' => '',
              'items_wrap' => '%3$s',
              'container' => '',
              "container_class" => '',
            ) ); ?>
          </ul>
        </div>
        <div class="col-md-4 col-xl-2 py-10 mt-sm-5 ps-xl-10 ms-xl-7 fs-14 d-flex justify-content-md-end">
          <a href="https://madebymade.pl/" target="_blank" class="made hover-flip">
            made by
            <span class="hover-flip-inner">
              <span>made</span>
              <span>made</span>
            </span>
          </a>
        </div>
      </div>
    </div>
  </footer>
</div><!-- .page-container -->

<?php wp_footer(); ?>
</body>
</html>