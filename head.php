<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php get_template_part('inc/general/meta'); ?>
  <!--[if lte IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
  <![endif]-->

  <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?= get_template_directory_uri(); ?>/img/favicons/apple-touch-icon-57x57.png?v=1.01" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?= get_template_directory_uri(); ?>/img/favicons/apple-touch-icon-114x114.png?v=1.01" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= get_template_directory_uri(); ?>/img/favicons/apple-touch-icon-72x72.png?v=1.01" />
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?= get_template_directory_uri(); ?>/img/favicons/apple-touch-icon-144x144.png?v=1.01" />
  <link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?= get_template_directory_uri(); ?>/img/favicons/apple-touch-icon-60x60.png?v=1.01" />
  <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?= get_template_directory_uri(); ?>/img/favicons/apple-touch-icon-120x120.png?v=1.01" />
  <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?= get_template_directory_uri(); ?>/img/favicons/apple-touch-icon-76x76.png?v=1.01" />
  <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?= get_template_directory_uri(); ?>/img/favicons/apple-touch-icon-152x152.png?v=1.01" />
  <link rel="icon" type="image/png" href="<?= get_template_directory_uri(); ?>/img/favicons/favicon-196x196.png?v=1.01" sizes="196x196" />
  <link rel="icon" type="image/png" href="<?= get_template_directory_uri(); ?>/img/favicons/favicon-96x96.png?v=1.01" sizes="96x96" />
  <link rel="icon" type="image/png" href="<?= get_template_directory_uri(); ?>/img/favicons/favicon-32x32.png?v=1.01" sizes="32x32" />
  <link rel="icon" type="image/png" href="<?= get_template_directory_uri(); ?>/img/favicons/favicon-16x16.png?v=1.01" sizes="16x16" />
  <link rel="icon" type="image/png" href="<?= get_template_directory_uri(); ?>/img/favicons/favicon-128.png?v=1.01" sizes="128x128" />
  <meta name="application-name" content="><?= get_bloginfo( 'name' ); ?>"/>
  <meta name="theme-color" content="#1c5de0">
  <meta name="msapplication-TileColor" content="#1c5de0" />
  <meta name="msapplication-TileImage" content="<?= get_template_directory_uri(); ?>/img/favicons/mstile-144x144.png?v=1.01" />
  <meta name="msapplication-square70x70logo" content="<?= get_template_directory_uri(); ?>/img/favicons/mstile-70x70.png?v=1.01" />
  <meta name="msapplication-square150x150logo" content="<?= get_template_directory_uri(); ?>/img/favicons/mstile-150x150.png?v=1.01" />
  <meta name="msapplication-wide310x150logo" content="<?= get_template_directory_uri(); ?>/img/favicons/mstile-310x150.png?v=1.01" />
  <meta name="msapplication-square310x310logo" content="<?= get_template_directory_uri(); ?>/img/favicons/mstile-310x310.png?v=1.01" />
  
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> >
  <div class="page-container" data-scroll-container>
    <div class="overlay">
      <span></span>
    </div>
    <div class="overlay-menu">
      <span></span>
    </div>
    <div class="overlay-lightbox">
      <a href="#" class="lightbox-close hover-skew js-lightbox-close">
        <span class="lightbox-close-inner exclude">
          <span class="exclude"></span>
          <span class="exclude"></span>          
        </span>
      </a>
      <span></span>
    </div>