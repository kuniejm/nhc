const path = require('path');
const webpack = require('webpack');
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

module.exports = {
	mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
	entry: [ './src/js/main.js', './src/sass/style.scss' ],
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'bundle.js'
	},
	module: {
		rules: [
			{
				test: /\.scss$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].css'
						}
					},
					{
						loader: 'extract-loader'
					},
					{
						loader: 'css-loader?-url'
					},
					{
						loader: 'postcss-loader'
					},
					{
						loader: 'sass-loader'
					}
				]
			},
			{
				test: /\.(otf|woff2?|ttf|woff|otf|eot|svg)$/,
				exclude: /node_modules/,
				loader: 'file-loader',
				options: {
					name: 'fonts/[name].[ext]'
				}
			}
		]
	},
	plugins: [
		new MomentLocalesPlugin({
			localesToKeep: [ 'pl' ]
		}),
		// new BrowserSyncPlugin({
		// 	files: '**/*.php',
		// 	notify: false,
		// 	proxy: 'http://localhost:8888/nhc/'
		// })
	],
	resolve: {
		alias: {
			//Waypoints: path.resolve('node_modules', 'waypoints/lib/jquery.waypoints.js'),
			//Selectric: path.resolve('node_modules', 'selectric/public/jquery.selectric.min.js')
		}
	}
};
