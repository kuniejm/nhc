<?php 
	$j = $args['j'];
	$id = $args['id'];
	$total = $args['total'];
	$url = get_permalink($id);
	$img = get_the_post_thumbnail_url($id, 'full');
?>
<div class="line-decorative" data-scroll></div>
<div class="container-fluid blog-list-item pt-10 mt-5 mt-md-0">
	<div class="row">
		<div class="col-md-6 col-xl-4 pe-sm-0 offset-xl-2 mb-10 mb-md-0">
			<div class="text-blue fs-23 mb-10">
				<?php if ($j <= 9): ?>0<?php endif; ?><?= $j . ' / ' . $total; ?>
			</div>
			<h3 class="h3-medium lh-1 mb-5"><a href="<?= $url; ?>" class="hover-blue js-custom-exit" data-splitting data-scroll><?= get_the_title($id); ?></a></h3>
			<div data-scroll data-scroll-opacity>
				<div class="paragraph pb-10">
					<?php the_excerpt(); ?>
				</div>
				<a href="<?= $url; ?>" class="mt-sm-8 col-12 col-sm-10 hover-line fs-15 js-custom-exit"><?php _e('read full story', 'nhfc'); ?>
					<svg enable-background="new 0 0 28.7 9.5" width="38" height="12" viewBox="0 0 28.7 9.5" xmlns="http://www.w3.org/2000/svg"><path d="m.8 4.8h27.2m-.1 0-4-4m4 4-4 4" fill="none" stroke="#1c5de0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
				</a>				
			</div>
		</div>
		<div class="col-md-5 position-relative offset-md-1 pe-0">
			<div class="line-vertical" data-scroll></div>
			<a href="<?= $url; ?>" class="hover-zoom js-custom-exit" data-scroll data-scroll-img>
				<img src="<?= $img; ?>" alt="">
			</a>
		</div>
	</div>
</div>