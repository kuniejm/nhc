<?php 
	$j = $args['j'];
	$id = $args['id'];
	$url = get_permalink($id);
	$title = get_the_title($id);
	$date = get_the_date('j F Y');
	$img = get_the_post_thumbnail_url($id, 'full');
?>
<div class="col-md-6 col-xl-4 pb-100 news-list-item<?php if ($j % 2 == 0): ?> offset-xl-2<?php else: ?> offset-xl-1<?php endif; ?>">

	<a href="<?= $url; ?>" class="hover-zoom mb-8 js-custom-exit" data-scroll data-scroll-img>
		<img src="<?= $img; ?>" alt="<?= strip_tags($title); ?>">
	</a>

	<div class="text-blue text-uppercase fs-14 mb-7">
		<?= $date; ?>
	</div>
	<h2 class="mb-5 lh-1"><a href="<?= $url; ?>" class="hover-blue js-custom-exit" data-splitting data-scroll><?= $title; ?></a></h2>
	<div data-scroll data-scroll-opacity>
		<a href="<?= $url; ?>" class="hover-line col-lg-6 fs-14 js-custom-exit"><?php _e('read more', 'nhfc'); ?>
			<svg enable-background="new 0 0 28.7 9.5" width="30" height="10" viewBox="0 0 28.7 9.5" xmlns="http://www.w3.org/2000/svg"><path d="m.8 4.8h27.2m-.1 0-4-4m4 4-4 4" fill="none" stroke="#1c5de0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
		</a>				
	</div>
</div>