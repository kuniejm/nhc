<?php 
	$id = $args['id'];
	$url = get_field('url', $id);
	$logo = get_field('hero_img', $id);
	$img = get_the_post_thumbnail_url($id, 'full');
?>
<div class="line-decorative mt-10 mt-md-0" data-scroll></div>
<div class="container-fluid partner-list-item pt-10 mt-5 mt-md-0">
	<div class="row">
		<div class="col-md-6 col-xl-4 pe-sm-0 offset-xl-2 mb-10 mb-md-0">
			<h3 class="h3-medium lh-1 mb-10" data-scroll data-splitting>
				<?= get_the_title($id); ?>
			</h3>
			<div data-scroll data-scroll-opacity>
				<div class="paragraph fs-18">
					<?php the_content(); ?>
				</div>
				<?php if ($url): ?>
				<a href="<?= $url; ?>" class="mt-sm-8 col-12 col-sm-10 hover-line fs-15"><?php _e('website', 'nhfc'); ?>
					<svg enable-background="new 0 0 28.7 9.5" width="38" height="12" viewBox="0 0 28.7 9.5" xmlns="http://www.w3.org/2000/svg"><path d="m.8 4.8h27.2m-.1 0-4-4m4 4-4 4" fill="none" stroke="#1c5de0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
				</a>	
				<?php endif; ?>			
			</div>
		</div>
		<div class="col-md-6 position-relative ps-md-10 pe-0">
			<div class="line-vertical" data-scroll></div>
			<?php if ($url): ?>
				<a href="<?= $url; ?>" class="hover-zoom" data-scroll data-scroll-img>
					<img src="<?= $img; ?>" alt="">
				</a>
			<?php else: ?>
				<div data-scroll data-scroll-img>
					<img src="<?= $img; ?>" alt="">
				</div>
			<?php endif; ?>

			<?php if ($logo): ?>
				<div class="mt-10 pt-sm-2 partner-list-item-logo">
					<img src="<?= $logo['url']; ?>" alt="<?= $logo['alt']; ?>" data-scroll data-scroll-opacity>					
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>