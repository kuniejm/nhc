<?php  
	$img_menu = get_field('menu_img', 'options');
	$contact = get_field('contact', 'options');

	if (is_front_page()) {
		$title = get_bloginfo( 'name' );
	} else if (is_singular('news')) {
		$title = __('News & publications');
	} else {
		if (is_home() || is_singular('post')) {
			$id = get_option( 'page_for_posts' );
		} else {
			$id = get_the_ID();
		}
		$title = get_the_title($id);
	}
?>
<section class="nav-panel d-none d-md-flex<?php if (is_front_page()): ?> is-transparent<?php endif; ?>">
	<div class="nav-panel-vertical ls-1"><?= $title; ?>

	</div>		
	<?php if (is_singular('post')): ?>
		<div class="nav-panel-single text-blue d-none d-md-flex"><span><?= getPostNumber(get_the_ID()); ?></span></div>
	<?php endif; ?>
</section>

<section class="nav nav-main">
	<input type="hidden" class="nav-input position-absolute">
	<div class="container-fluid text-white px-xl-0 h-100" tabindex="0">
		<div class="row flex-lg-nowrap mx-0 align-items-lg-end h-100">
			<div class="nav-main-header ps-8 ps-xl-0 px-xl-0 w-auto d-none d-md-flex">
				<div class="col-6 col-lg-4 col-xl-3 offset-xl-2 col-text">
					<a href="<?= home_url('/'); ?>" class="logo d-inline-block js-custom-exit">
						<img src="<?= get_template_directory_uri(); ?>/img/static/logo-white.svg" alt="">
					</a>	
				</div>	
				<div class="col-auto pe-0 ms-auto header-contact h-100 d-none d-lg-block">
					<a href="<?= get_permalink($contact); ?>" title="<?= get_the_title($contact); ?>" class="fs-14 hover-skew px-7 d-inline-flex align-items-center h-100 text-uppercase js-custom-exit">
						<span class="position-relative zIndex2">
						<?= get_the_title($contact); ?></span>
					</a>						
				</div>
			</div>
			<div class="nav-main-text px-5 px-lg-0 pe-lg-4">
				<ul class="list-unstyled" data-splitting>
					<?php wp_nav_menu( array( 
						'theme_location' => 'main_menu', 
						'menu_class' => '',
						'items_wrap' => '%3$s',
						'container' => '',
						"container_class" => '',
					) ); ?>
				</ul>
			</div>

			<?php if ($img_menu): ?>
				<div class="nav-main-img position-relative">
					<div class="h1" data-splitting>The Journey <br>to Carbon <br>Net Zero</div>
					<div class="bg h-100" style="background-image: url('<?= $img_menu; ?>');"></div>
				</div>
			<?php endif; ?>

		</div>
	</div>
</section>