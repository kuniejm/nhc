<?php
	$lat = $args['lat'];
	$lng = $args['lng'];
	$args = array( 
		'posts_per_page' => -1,
		'post_type' => 'nordic-hydrogen',
		'orderby' => 'menu_order',
		'order' => 'ASC',
		'fields' => 'ids',
		'post_status' => array('publish')
	);

  $query = new WP_Query($args);   
?>

<?php if ($query->have_posts()): ?>
<section data-scroll-section class="section-map position-relative">
	<div class="slider-map-container is-active is-inactive mb-10 mb-lg-0" data-scroll data-scroll-opacity>
		<div class="swiper-container slider-map">
			<div class="map-filters d-flex flex-column fs-14 lh-1 mt-7 mx-6">
				<a href="#" data-cat="cat0" class="d-inline-flex align-items-center mb-5">
					<img src="<?= get_template_directory_uri(); ?>/img/icons/marker-blue.svg" alt="" class="me-4 me-sm-5">
					<span><?php _e('signed agreement', 'nhc'); ?></span>
				</a>
				<a href="#" data-cat="cat1" class="d-inline-flex align-items-center mb-5">
					<img src="<?= get_template_directory_uri(); ?>/img/icons/marker-light.svg" alt="" class="me-4 me-sm-5">
					<span><?php _e('signed MOU', 'nhc'); ?></span>
				</a>
				<a href="#" data-cat="cat2" class="d-inline-flex align-items-center mb-5">
					<img src="<?= get_template_directory_uri(); ?>/img/icons/marker-pink.svg" alt="" class="me-4 me-sm-5">
					<span><?php _e('started operation', 'nhc'); ?></span>
				</a>
			</div>
			<div class="swiper-wrapper">
				<?php while ( $query->have_posts() ) : $query->the_post(); 
						$id = get_the_ID(); 
						$latlng = get_field('latlng', $id);
						$cat = get_field('category', $id);
					?>
					<div class="swiper-slide" data-cat="<?= $cat; ?>" data-lat="<?= $latlng['lat']; ?>" data-lng="<?= $latlng['lng']; ?>">
						<div class="slider-map-title">
							<?= get_the_title($id); ?>
						</div>
						<div class="paragraph fs-15">
							<?= get_field('address', $id); ?>
						</div>
					</div>
				<?php endwhile; wp_reset_postdata(); ?>
			</div>
			<div class="d-flex px-6 pb-3">
				<a href="#" class="swiper-button swiper-button-prev">
					<svg enable-background="new 0 0 28.7 9.5" width="38" height="12" viewBox="0 0 28.7 9.5" xmlns="http://www.w3.org/2000/svg"><path d="m.8 4.8h27.2m-.1 0-4-4m4 4-4 4" fill="none" stroke="#1c5de0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
				</a>
				<a href="#" class="swiper-button swiper-button-next">
					<svg enable-background="new 0 0 28.7 9.5" width="38" height="12" viewBox="0 0 28.7 9.5" xmlns="http://www.w3.org/2000/svg"><path d="m.8 4.8h27.2m-.1 0-4-4m4 4-4 4" fill="none" stroke="#1c5de0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
				</a>
			</div>
		</div>		
	</div>
	<div class="map w-100 bg-gray map1" 
	  data-center-lat="<?= $lat; ?>"
	  data-center-lng="<?= $lng; ?>"
		data-current="<?= get_template_directory_uri(); ?>/img/icons/marker-current.svg"
		data-marker="<?= get_template_directory_uri(); ?>/img/icons/marker-blue.svg">
	</div>
	<div class="map-controls d-flex flex-column">
		<a href="#" tabindex="-1" class="map-controls-zoom hover-skew"><img src="<?= get_template_directory_uri(); ?>/img/icons/plus.svg" alt="+"></a>
		<a href="#" tabindex="-1" class="map-controls-zoomout hover-skew"><img src="<?= get_template_directory_uri(); ?>/img/icons/minus.svg" alt="-"></a>
	</div>
</section>
<?php endif; ?>