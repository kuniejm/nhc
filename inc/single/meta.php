<section data-scroll-section>
  <div class="container-fluid py-10 fs-20" data-scroll data-scroll-opacity>
    <div class="row">
      <?php if (!is_singular('news')): ?>
      <div class="col-12 mb-2 mb-sm-0 col-sm-6 col-md-3 offset-xl-2 hover-underline">
        <?php 
        $cats = wp_get_post_categories( get_the_ID(), array( 'fields' => 'ids' ) );

        if ($cats):
          foreach ($cats as $term_id): ?>
            <a class="me-1 d-inline-block text-uppercase my-1" href="<?= esc_url( get_category_link( $term_id ) ); ?>"><?= get_cat_name($term_id); ?></a>
          <?php endforeach; ?>
        <?php endif; ?>  

      </div>
    <?php endif; ?>
      <?php if (get_field('time')): ?>
      <div class="col-12 mb-2 mb-sm-0 col-sm-6 col-md-6 d-inline-flex align-items-center<?php if (is_singular('news')): ?>  offset-xl-2<?php endif; ?>">
        <img src="<?= get_template_directory_uri(); ?>/img/icons/clock.svg" alt="" class="me-5 my-1">
        <?= get_field('time'); ?>
      </div>   
      <?php endif; ?>   
    </div>
  </div>
</section>