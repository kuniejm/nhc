<?php 
	$video = get_field('video'); 
	$h1 = get_field('text'); 
?>
<section class="section-hero" data-scroll-section>
	<div class="w-100 h-100 section-hero-inner" data-scroll data-scroll-img data-scroll-speed="-.5">
		<video id="player-home" class="player" autoplay controls="0" playsinline loop muted="1">
			<source src="<?= $video; ?>" type="video/mp4" class="w-100" data-scroll data-scroll-speed=".4" class="img-fit" />
		</video>		
	</div>
	<div class="container-fluid text-white">
		<div class="row h-100">	
			<div class="col-12 col-md-10 offset-xl-2 position-relative">
				<h1 data-scroll data-splitting><?= $h1; ?></h1>	
				<a href="#section0" class="hover-arrow w-auto me-auto mt-auto pb-1" data-scroll-to data-scroll data-scroll-opacity>
					<svg enable-background="new 0 0 78 78" viewBox="0 0 78 78" xmlns="http://www.w3.org/2000/svg"><g fill="none" stroke="#ffffff"><circle cx="39" cy="39" r="38" stroke-width="2"></circle><path d="m39 25.4v27.2m0 0 4-4m-4 4-4-4" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"></path></g></svg>
					<?php _e('Find out more', 'nhc'); ?>
				</a>				
			</div>
		</div>
	</div>
</section>