<?php

$current = get_the_ID();
$args = array(
	'post_type' => 'news',
	'post_status' => 'publish',
	'posts_per_page' => 2,
	'post__not_in'           => array($current),
	'orderby'        => 'date', 
	'order'          => 'DESC' 
); 
$the_query = new WP_Query( $args );
if ( $the_query->have_posts() ) : 
?>
	<section class="section-newses" data-scroll-section>
		<div class="line-decorative" data-scroll></div>
		<div class="container-fluid pt-7">
			<div class="row pb-100">			
				<h2 class="col-lg-6 col-xl-4 offset-xl-2" data-splitting data-scroll><?php _e('Latest news', 'nhc'); ?></h2>
				<div class="col-12 d-none d-lg-block col-lg-6 col-xl-5 offset-xl-1" data-scroll data-scroll-opacity>
					<a href="<?= home_url(); ?>/news-publications/" class="hover-line fs-13 mb-7 js-custom-exit"><?php _e('SEE ALL NEWS & PUBLICATIONS', 'nhfc'); ?>
						<svg enable-background="new 0 0 28.7 9.5" width="30" height="10" viewBox="0 0 28.7 9.5" xmlns="http://www.w3.org/2000/svg"><path d="m.8 4.8h27.2m-.1 0-4-4m4 4-4 4" fill="none" stroke="#1c5de0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
					</a>		
				</div>
			</div>
			<div class="row">
       <?php 
				$j = 0; 
				while ( $the_query->have_posts() ) : $the_query->the_post(); 
					get_template_part('inc/components/news-list-item', null, array(
						'id' => get_the_ID(),
						'j' => $j
					)); 
				$j++; 
			 endwhile; ?>

				<div class="col-12 d-flex justify-content-end d-lg-none pb-100" data-scroll data-scroll-opacity>
					<a href="<?= home_url(); ?>/news-publications/" class="hover-line fs-13 mb-7 js-custom-exit"><?php _e('SEE ALL NEWS & PUBLICATIONS', 'nhfc'); ?>
						<svg enable-background="new 0 0 28.7 9.5" width="30" height="10" viewBox="0 0 28.7 9.5" xmlns="http://www.w3.org/2000/svg"><path d="m.8 4.8h27.2m-.1 0-4-4m4 4-4 4" fill="none" stroke="#1c5de0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
					</a>		
				</div>
		</div>
	</div>
</section>
<?php wp_reset_query(); endif; ?>