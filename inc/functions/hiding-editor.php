<?php 

// add_action( 'admin_head', 'hide_editor' );
// function hide_editor() {
//   $template_file = basename( get_page_template() );
//   if($template_file == 'festival.php' || $template_file == 'eu-projects.php' ||
//     $template_file == 'workshops.php'
//   ){
//     remove_post_type_support('page', 'editor');
//   }
//   if((int) get_option('page_on_front')==get_the_ID()) {
//       remove_post_type_support('page', 'editor');
//   }
// }


add_filter( 'use_block_editor_for_post', 'beauty_hide_editor', 10, 2 );
function beauty_hide_editor( $use_block_editor, $post_type ) {
  remove_post_type_support( 'page', 'editor' );
  remove_post_type_support( 'post', 'editor' );
  return false; 
}