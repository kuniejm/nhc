<?php

function custom_excerpt_length( $length ) {
  return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function wpdocs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

//always show excerpt
add_action('admin_head', 'myplugin_modify_admin_header');
function myplugin_modify_admin_header() {
  ?>
  <style type='text/css'> 
    .post-type-post #postexcerpt { display: block !important; } 
    label[for=postexcerpt-hide] { display: none !important; }
  </style>
  <?php
}

//move excerpt to the top
add_action( 'edit_form_after_title', 'move_excerpt_meta_box' );
function move_excerpt_meta_box( $post ) {
  if ( post_type_supports( $post->post_type, 'excerpt' ) ) {
      remove_meta_box( 'postexcerpt', $post->post_type, 'normal' ); ?>
      <h2 style="padding: 20px 0 0;">Excerpt</h2>
      <?php post_excerpt_meta_box( $post );
  }
}