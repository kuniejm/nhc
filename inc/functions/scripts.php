<?php 

function custom_enqueue_script() {
  $addGooge = false;
  /*
   * load scripts based on the modules that exist on a page
  */
  if ( class_exists('acf') ):
    if (is_home()) {
      $id = get_option( 'page_for_posts' );
    } else {
      $id = get_the_ID();
    }

    if( have_rows('sections', $id) ):
      while ( have_rows('sections', $id) ) : the_row(); 

        if ( (get_row_layout() == 'map_refuelling') || (get_row_layout() == 'map_nordic')):
          $addGooge = true;
            wp_enqueue_script( 'google-map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAXQG_ZIDm-ILtkXqREebKb8pHSYSzUSMQ', '', '', false );
        endif;
      endwhile;
    endif;
  endif;

  if ($addGooge) {
    wp_enqueue_script( 'main-js', get_stylesheet_directory_uri() . '/dist/bundle.js#asyncload', array('google-map'), '20220124' , true );
  } else {
    wp_enqueue_script( 'main-js', get_stylesheet_directory_uri() . '/dist/bundle.js#asyncload', array(), '20220124' , true );
  }


}
add_action( 'wp_enqueue_scripts', 'custom_enqueue_script' );


function my_enqueue_style() {   
  wp_dequeue_style( 'wp-block-library' );
  wp_dequeue_style( 'wp-block-library-theme' );
  wp_enqueue_style( 'main-css', get_stylesheet_directory_uri() . '/dist/style.css', array(), '20220123');
}
add_action( 'wp_enqueue_scripts', 'my_enqueue_style' );


add_editor_style(get_stylesheet_directory_uri() . '/editor.css', 100);
