<?php 

function custom_image_size() {
  update_option('image_default_align', 'center' );
  update_option('image_default_size', 'full' );

}
add_action('after_setup_theme', 'custom_image_size');

function add_custom_sizes() {
 add_image_size( 'img_450_240', 450, 240, true ); 
 add_image_size( 'img_780_525', 780, 525, true ); 
 add_image_size( 'full_hd', 1920, 1080, true ); 
}
add_action('after_setup_theme','add_custom_sizes');