<?php 

// remove comments from menu
add_action( 'admin_menu', 'remove_from_menu' );
function remove_from_menu() {
  remove_menu_page('edit-comments.php');
}

add_action('init', 'remove_comment_support', 100);
function remove_comment_support() {
    remove_post_type_support( 'post', 'comments' );
    remove_post_type_support( 'page', 'comments' );
}

// remove comments from admin bar
function my_admin_bar_render() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
}
add_action( 'wp_before_admin_bar_render', 'my_admin_bar_render' );

add_action('after_setup_theme', 'custom_remove_admin_bar');
function custom_remove_admin_bar() {
  if (is_user_logged_in()) {
    show_admin_bar(false);
  }
}

add_filter('upload_mimes', 'cc_mime_types');
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}

add_theme_support( 'post-thumbnails', array( 'post', 'partner', 'news' ) );

add_action( 'init', '_unregister_taxonomy' );
function _unregister_taxonomy() {
  global $wp_taxonomies;
  if ( taxonomy_exists('post_tag') )
    unset( $wp_taxonomies['post_tag'] );
}

remove_action('wp_head', 'wp_generator');

add_filter('wpseo_metabox_prio', function($prio) {
  return 'low';
}, 10, 1);


function add_async_forscript($url) {
  if (strpos($url, '#asyncload')===false)
    return $url;
  else if (is_admin())
    return str_replace('#asyncload', '', $url);
  else
    return str_replace('#asyncload', '', $url)."' async defer"; 
}
add_filter('clean_url', 'add_async_forscript', 11, 1);


function my_mce4_options($init) {

  $custom_colours = '
  "040a5c", "Navy",
  "1c5de0", "Blue",
  "f1ede9", "Gray",
  "ffffff", "White",
  ';
  $init['textcolor_map'] = '['.$custom_colours.']';
  $init['textcolor_rows'] = 1;

  return $init;
}
add_filter('tiny_mce_before_init', 'my_mce4_options');

function my_acf_color_picker_palette_primary() {
  
?>
<script type="text/javascript">
(function($) {
  
  acf.add_filter('color_picker_args', function( args, $field ){
  
  // find a specific field
  var $field = $('color_primary');  
    
  // do something to args
  args.palettes = ['#040a5c', '#ffffff', '#f1ede9', '#1c5de0']
  
  
  // return
  return args;
      
});
  
})(jQuery); 
</script>
<?php
    
}

add_action('acf/input/admin_footer', 'my_acf_color_picker_palette_primary');


function theme_slug_setup() {
    add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'theme_slug_setup' );

function my_mce_buttons_2($buttons) { 
  $buttons[] = 'superscript';
  $buttons[] = 'subscript';

  return $buttons;
}
add_filter('mce_buttons_2', 'my_mce_buttons_2');

add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );
function remove_dashboard_widgets() {
    global $wp_meta_boxes;
    unset($wp_meta_boxes['dashboard']);

}

function wpdocs_unregister_tags_for_posts() {
    unregister_taxonomy_for_object_type( 'post_tag', 'post' );
}
add_action( 'init', 'wpdocs_unregister_tags_for_posts' );


/*remove term descriptions from post editor */
add_action( 'admin_head-term.php', 'wpse_hide_cat_descr' );
add_action( 'admin_head-edit-tags.php', 'wpse_hide_cat_descr' );
function wpse_hide_cat_descr() { ?>
    <style type="text/css">
       .term-description-wrap {
           display: none;
       }
    </style>

<?php } 


//wrap iframe in bootstrap div
add_filter('embed_oembed_html', 'my_embed_oembed_html', 99, 4);
function my_embed_oembed_html($html, $url, $attr, $post_id) {
  return '<div class="ratio ratio-16x9">' . $html . '</div>';
}

/**
 * Filter for adding wrappers around embedded objects
 */
function responsive_embeds( $content ) {
  $content = preg_replace( "/<object/Si", '<div class="ratio ratio-16x9"><object', $content );
  $content = preg_replace( "/<\/object>/Si", '</object></div>', $content );
  
  /**
   * Added iframe filtering, iframes are bad.
   */
  $content = preg_replace( "/<iframe.+?src=\"(.+?)\"/Si", '<div class="ratio ratio-16x9"><iframe src="\1" frameborder="0" allowfullscreen>', $content );
  $content = preg_replace( "/<\/iframe>/Si", '</iframe></div>', $content );

  return $content;
}

add_filter( 'the_content', 'responsive_embeds' );


function checkIfDev() {
  $url = get_permalink(); 
  if (strpos($_SERVER['REQUEST_URI'], "madebymade") == true || strpos($_SERVER['REQUEST_URI'], "localhost") == false) {
    return true;
  }
  return false;
}

add_filter( 'image_send_to_editor', 'wp_image_wrap_init', 10, 8 );  
  function wp_image_wrap_init( $html, $id, $caption, $title, $align, $url, $size, $alt ) {
    if ($url) {
      $imgUrl = $url;
      $return = '<a href="' . $imgUrl .'" class="d-inline-block" data-scroll data-scroll-img>'. $html .'</a>';
    } else {
      $return = '<span class="d-inline-block" data-scroll data-scroll-img>'. $html .'</span>';
    }
    
    return $return;
}


add_filter('acf/format_value/type=wysiwyg', 'format_value_wysiwyg', 10, 3);
function format_value_wysiwyg( $value, $post_id, $field ) {
  $value = apply_filters( 'the_content', $value );
  return $value;
}

function getPostNumber($id) {
  $total = wp_count_posts()->publish;
  if ($total <= 9) {
    $total = '0' . $total;
  }
  $posts = get_posts(array(
    'fields' => 'ids',
    'post_type'        => 'post',
  ));

  if (!empty($posts)) {
    $index = array_search($id, $posts);
  }
  $index += 1;
  if ($index <= 9) {
    $index = '0' . $index;
  }

  return $index . ' / ' . $total;
}

function change_post_menu_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Connecting the Nordic';
    $submenu['edit.php'][5][0] = 'Connecting the Nordic';
    $submenu['edit.php'][10][0] = 'Add new';
    echo '';
}
add_action( 'admin_menu', 'change_post_menu_label' );

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );