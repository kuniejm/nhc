<?php 

add_action( 'init', 'register_nav' );
function register_nav() {
  register_nav_menus(
    array( 
      'main_menu' => __( 'Main menu' ),
      'footer_menu' => __( 'Footer menu' )
    )
  );
}


function add_specific_menu_location_atts( $atts, $item, $args ) {
    //if( $args->menu == 'main_menu' ) {
      // add the desired attributes:
      $atts['class'] = ' js-custom-exit';
    //}
    return $atts;
}
add_filter( 'nav_menu_link_attributes', 'add_specific_menu_location_atts', 10, 3 );


add_filter( 'walker_nav_menu_start_el', 'nhc_add_submenu_plus',10,4);
function nhc_add_submenu_plus( $item_output, $item, $depth, $args ){
  if('main_menu' == $args->theme_location && $depth == 0){
  if (in_array('menu-item-has-children', $item->classes)) {
      $item_output .='<a href="#" class="toggle-submenu"><svg viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg" width="26" height="26"><path d="M7.5 1v13M1 7.5h13" stroke="currentColor" stroke-width=".5"></path></svg></a>';
    }
  }
  return $item_output;
}
