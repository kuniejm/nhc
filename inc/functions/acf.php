<?php 

if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(array(
    'page_title'  => 'Options',
    'menu_title'  => 'Options',
    'menu_slug'   => 'footer',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
}


/*
===============================
ACF API key
===============================
*/
function my_acf_init() {
  acf_update_setting('google_api_key', 'AIzaSyAXQG_ZIDm-ILtkXqREebKb8pHSYSzUSMQ');
}
add_action('acf/init', 'my_acf_init');