<div class="col-md-7 col-xl-5 pb-100">
	<?php if( have_rows('links') ): ?>
		<?php while( have_rows('links') ) : the_row(); ?>
			<?php 
			$link = get_sub_field('link');
			if( $link ):
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
			?>
			<a href="<?= esc_url( $link_url ); ?>" target="<?= esc_attr( $link_target ); ?>" class="hover-line hover-line-light w-100<?php if ($link['target'] == '_self'): ?> js-custom-exit<?php endif; ?>">
				<?= $link_title; ?>
				<svg enable-background="new 0 0 28.7 9.5" width="38" height="12" viewBox="0 0 28.7 9.5" xmlns="http://www.w3.org/2000/svg"><path d="m.8 4.8h27.2m-.1 0-4-4m4 4-4 4" fill="none" stroke="#1c5de0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
			</a>
		<?php endif; ?>
	<?php endwhile; ?>
<?php endif; ?>
</div>