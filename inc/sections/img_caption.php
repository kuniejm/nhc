<?php
	$i = $args['index'];
	$img = get_sub_field('img');
	$caption = get_sub_field('caption');
?>
<section id="section<?= $i; ?>" data-scroll-section>
	<div class="container-fluid pb-10">
		<div class="row">
			<figure class="col-md-8 mx-auto">
				<span class="d-block" data-scroll data-scroll-img data-scroll-offset="300">
					<img src="<?= $img['sizes']['full_hd']; ?>" alt="" class="w-100">
				</span>
				
				<figcaption data-scroll data-scroll-opacity><?= $caption; ?></figcaption>
			</figure>
		</div>
	</div>
</section>