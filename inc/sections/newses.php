<?php
$posts_per_page = 4;
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : '1';
$args = array(
	'post_type' => 'news',
	'post_status' => 'publish',
	'posts_per_page' => $posts_per_page,
	'paged'          => $paged, 
	'orderby'        => 'date', 
	'order'          => 'DESC' 
); 
$the_query = new WP_Query( $args );
$total = $the_query->found_posts;
?>
	<section class="section-newses" data-scroll-section>
		<div class="container-fluid">
			<div class="row">
       <?php 
       if ( $the_query->have_posts() ) : 
				$j = 0; 
				while ( $the_query->have_posts() ) : $the_query->the_post(); 
					get_template_part('inc/components/news-list-item', null, array(
						'id' => get_the_ID(),
						'j' => $j
					)); 
				$j++; 
			  endwhile; ?>
			  <?php if ($total > $posts_per_page): //if pagination needed?>
						<div class="col-12 pb-100">
							<div class="pagination d-flex justify-content-center">
								<?php 
								echo paginate_links( array(
									'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
									'total'        => $the_query->max_num_pages,
									'current'      => max( 1, get_query_var( 'paged' ) ),
									'format'       => '?paged=%#%',
									'show_all'     => false,
									'type'         => 'plain',
									'end_size'     => 2,
									'mid_size'     => 1,
									'prev_next'    => true,
									'prev_text'    => '<svg enable-background="new 0 0 28.7 9.5" width="38" height="12" viewBox="0 0 28.7 9.5" xmlns="http://www.w3.org/2000/svg"><path d="m.8 4.8h27.2m-.1 0-4-4m4 4-4 4" fill="none" stroke="#1c5de0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>',
									'next_text'    => '<svg enable-background="new 0 0 28.7 9.5" width="38" height="12" viewBox="0 0 28.7 9.5" xmlns="http://www.w3.org/2000/svg"><path d="m.8 4.8h27.2m-.1 0-4-4m4 4-4 4" fill="none" stroke="#1c5de0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>',
									'add_args'     => false,
									'add_fragment' => '',
								) );
								?>								
							</div>
						</div>
					<?php endif; ?>
			  <?php else : ?>
				<div class="col-md-8 offset-xl-2 paragraph paragraph-big pb-100">
					<p><?php esc_html_e( 'Sorry, no posts found.', 'nhc' ); ?></p>
				</div>
			<?php wp_reset_query(); endif; ?>
		</div>
	</div>
</section>