<section class="section-blog" data-scroll-section>
	<div class="container-fluid">
		<div class="row">
			<h2 class="h2-medium col-md-10 lh-1 offset-xl-2 pt-10 pb-7" data-splitting data-scroll><?php _e('Discover NHC stories', 'nhc'); ?></h2>
		</div>
	</div>
	<?php 
	  $total = wp_count_posts()->publish;
	  if ($total <= 9) {
	    $total = '0' . $total;
	  }
		if ( have_posts() ) : $j = 1; while ( have_posts() ) : the_post(); 
		 get_template_part('inc/components/blog-list-item', null, array(
		 	'id' => get_the_ID(),
		 	'j' => $j,
		 	'total' => $total
		 )); 
		$j++; endwhile; else : 
	?>
		<div class="container-fluid">
			<div class="col-md-8 offset-xl-2 paragraph paragraph-big">
				<p><?php esc_html_e( 'Sorry, no posts found.', 'nhc' ); ?></p>
			</div>
		</div>
	<?php endif; ?>
</section>