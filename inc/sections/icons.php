<?php
	$i = $args['index'];
	$text = get_sub_field('text');
	$bg = get_sub_field('background');

?>
<section id="section<?= $i; ?>" class="section-icons" data-scroll-section<?php if ($bg): ?> style="background-color: <?= $bg; ?>"<?php endif; ?>>
	<div class="container-fluid pt-100<?php if ($bg): ?> pb-100<?php endif; ?>">
		<div class="row">
			<?php if ($text): ?>
				<div class="paragraph paragraph-extra col-12 col-md-7 offset-xl-2 mb-10 pb-md-7" data-scroll data-splitting><?= $text; ?></div>
			<?php endif; ?>
		</div>

		<?php if( have_rows('icons') ): ?>
		<div class="row" data-scroll data-scroll-opacity>
			<div class="col-12 col-xl-10 offset-xl-2">
				<div class="row">
						<?php $i = 0; while( have_rows('icons') ) : the_row(); ?>
							<div class="col-6 col-md-4 section-icons-item<?php if ($bg): ?> mb-10<?php else: ?> mb-10 mb-md-10<?php endif; ?>">
								<?php if (get_sub_field('icon')): ?>
									<div class="section-icons-icon" data-scroll>
										<svg version="1.1" width="42" height="42" viewBox="0 0 46 46" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px">
											<circle cx="23" cy="23" r="22.5" fill="none" stroke="#040a5c" stroke-width=".3"></circle>
										</svg>
										<img src="<?= get_sub_field('icon'); ?>" alt="" class="mx-auto d-block">
									</div>
								<?php endif; ?>
								<?php if (get_sub_field('text')): ?>
								<div class="paragraph mx-auto px-md-10 text-center fs-18 lh-1-3 hover-underline"><?= get_sub_field('text'); ?></div>
								<?php endif; ?>
							</div>
						<?php $i++; endwhile; ?>		
				</div>
			</div>
		</div>
		<?php endif; ?>			
	</div>
</section>