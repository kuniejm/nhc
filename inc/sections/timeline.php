<?php
$i = $args['index'];
$title = get_sub_field('title');
$start = get_sub_field('start');
?>

<?php if (have_rows('timeline')): ?>
	<section id="section<?= $i; ?>" class="section-timeline overflow-hidden" data-scroll-section>
		<div class="container-fluid pt-100">
			<div class="row">
				<div class="col-md-7 pt-md-3 col-xl-5 offset-xl-2 pb-10 mb-md-10">
					<h2 data-scroll data-splitting><?= $title; ?></h2>
				</div>
				<div class="col-12 col-md-5 pb-10 mb-md-10" data-scroll data-scroll-opacity>
					<div class="d-flex my-3 my-md-0 justify-content-start">
						<a href="#" class="swiper-button swiper-button-prev">
							<svg enable-background="new 0 0 28.7 9.5" width="38" height="12" viewBox="0 0 28.7 9.5" xmlns="http://www.w3.org/2000/svg"><path d="m.8 4.8h27.2m-.1 0-4-4m4 4-4 4" fill="none" stroke="#1c5de0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
						</a>
						<a href="#" class="swiper-button swiper-button-next">
							<svg enable-background="new 0 0 28.7 9.5" width="38" height="12" viewBox="0 0 28.7 9.5" xmlns="http://www.w3.org/2000/svg"><path d="m.8 4.8h27.2m-.1 0-4-4m4 4-4 4" fill="none" stroke="#1c5de0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
						</a>
					</div>
				</div>

				<div class="col-12" data-scroll data-scroll-opacity>
					<div class="swiper-container slider-phases" data-start="<?= $start ? $start : 0; ?>" data-space="20" data-loop="0" data-offset="1">
						<div class="swiper-wrapper">
							<?php $i = 1; while( have_rows('timeline') ) : the_row(); ?>
							<a href="#" class="swiper-slide h3 pb-7">
								<?= __('Phase ', 'nhc') . $i; ?> 
							</a>
						  <?php $i++; endwhile; ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="line-decorative text-blue" data-scroll></div>

		<div class="container-fluid pb-100">
			<div class="row">
				<div class="col-12 mt-8" data-scroll data-scroll-opacity>
					<div class="swiper-container slider-timeline" data-space="0" data-fade="1" data-loop="0">
						<div class="swiper-wrapper">		
						<?php $i = 1; while( have_rows('timeline') ) : the_row(); ?>
							<?php 
							$title = get_sub_field('title');
							$text = get_sub_field('text');
							$time = get_sub_field('time');
							$img = get_sub_field('img');
							?>
							<div class="swiper-slide">
								<div class="swiper-slide-time text-blue text-uppercase col-lg-5 offset-lg-5 offset-xl-7"><?= $time; ?></div>
								<div class="swiper-slide-title mb-5 mb-lg-0 col-lg-4 offset-lg-5 offset-xl-7"><?= $title; ?></div>
								<div class="row">
									<div class="col-lg-5 offset-xl-2">
										<img src="<?= $img; ?>" alt="<?= strip_tags($title); ?>" class="col-12 col-lg-10 px-0">
									</div>
									<div class="col-lg-5 col-xl-3 paragraph hover-underline">
										<?= $text; ?>
									</div>									
								</div>
							</div>
						<?php $i++; endwhile; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
</section>
<?php endif; ?>