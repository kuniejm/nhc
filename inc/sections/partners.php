<?php
$args = array(
  'post_type' => 'partner',
  'post_status' => 'publish',
  'posts_per_page' => -1,
  'fields' => 'ids',
  'orderby' => 'menu_order',
  'order' => 'ASC'
); 
$the_query = new WP_Query( $args ); ?>
<section class="section-partners" data-scroll-section>
  <?php 
  if ( $the_query->have_posts() ) : 
		while ( $the_query->have_posts() ) : $the_query->the_post(); 
		 get_template_part('inc/components/partner-list-item', null, array(
		 	'id' => get_the_ID()
		 )); 
		endwhile; 
	else : 
	?>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-8 offset-xl-2 paragraph paragraph-big">
					<p><?php esc_html_e( 'Sorry, no Partners found.', 'nhc' ); ?></p>
				</div>
			</div>
		</div>
	<?php endif; wp_reset_query(); ?>
</section>