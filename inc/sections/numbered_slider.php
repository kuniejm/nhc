<?php
$i = $args['index'];
$title = get_sub_field('title');
?>

<?php if (have_rows('slider')): ?>
	<section id="section<?= $i; ?>" class="section-numbered overflow-hidden bg-gray" data-scroll-section>
		<div class="container-fluid pt-100 pb-100">
			<div class="row pb-100">
				<div class="col-12 col-md-7 col-xl-5 offset-xl-2 pb-10">
					<h2 class="h2-medium" data-scroll data-splitting><?= $title; ?></h2>
				</div>
				<div class="col-12 col-md-5 col-xl-4 ps-xl-10 pb-md-10 ms-xl-7 mb-0 mb-md-3" data-scroll data-scroll-opacity>
					<div class="d-flex my-3 my-md-0 justify-content-start justify-content-md-end h-100 align-items-end">
						<a href="#" class="swiper-button swiper-button-prev">
							<svg enable-background="new 0 0 28.7 9.5" width="38" height="12" viewBox="0 0 28.7 9.5" xmlns="http://www.w3.org/2000/svg"><path d="m.8 4.8h27.2m-.1 0-4-4m4 4-4 4" fill="none" stroke="#1c5de0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
						</a>
						<a href="#" class="swiper-button swiper-button-next">
							<svg enable-background="new 0 0 28.7 9.5" width="38" height="12" viewBox="0 0 28.7 9.5" xmlns="http://www.w3.org/2000/svg"><path d="m.8 4.8h27.2m-.1 0-4-4m4 4-4 4" fill="none" stroke="#1c5de0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
						</a>
					</div>
				</div>
				<div class="col-12 col-md-8 offset-md-3 col-xl-7 offset-xl-5">
					<div class="swiper-container slider-text" data-space="40">
						<div class="swiper-wrapper">		
						<?php $i = 1; while( have_rows('slider') ) : the_row(); ?>
							<?php 
							$text = get_sub_field('text');
							?>
							<div class="swiper-slide">
								<div class="swiper-slide-num" data-scroll data-splitting>
									<?php if ($j <= 9): ?>0<?php endif; ?><?= $i; ?>
								</div>
								<div class="swiper-slide-plus text-blue">+</div>
								<div class="paragraph hover-underline" data-scroll data-scroll-opacity><?= $text; ?></div>
							</div>
						<?php $i++; endwhile; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
</section>
<?php endif; ?>