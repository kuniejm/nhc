<?php 
	$i = $args['index'];
	$id = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'template-newses.php',
    'posts_per_page' => 1
));
	$title = get_field('title', $id[0]);
?>
<section id="section<?= $i; ?>" class="section-more overflow-hidden" data-scroll-section>
	<div class="line-decorative" data-scroll></div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-5 d-flex flex-column justify-content-between col-xl-3 offset-xl-2 pt-7">
				<h2 data-splitting data-scroll><?= $title; ?></h2>
			</div>
			<div class="col-md-7 col-xl-5 pb-100">
				<?php if( have_rows('links', $id[0]) ): ?>
					<?php while( have_rows('links', $id[0]) ) : the_row(); ?>
						<?php 
						$link = get_sub_field('link');
						if( $link ):
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						?>
						<a href="<?= esc_url( $link_url ); ?>" target="<?= esc_attr( $link_target ); ?>" class="hover-line hover-line-light w-100<?php if ($link['target'] == '_self'): ?> js-custom-exit<?php endif; ?>">
							<?= $link_title; ?>
							<svg enable-background="new 0 0 28.7 9.5" width="38" height="12" viewBox="0 0 28.7 9.5" xmlns="http://www.w3.org/2000/svg"><path d="m.8 4.8h27.2m-.1 0-4-4m4 4-4 4" fill="none" stroke="#1c5de0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
						</a>
					<?php endif; ?>
				<?php endwhile; ?>
			<?php endif; ?>
			</div>
		</div>
	</div>
</section>