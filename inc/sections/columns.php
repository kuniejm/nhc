<?php
	$i = $args['index'];
	$col0 = get_sub_field('col0');
	$col1 = get_sub_field('col1');
	$text_left = get_sub_field('text_left');

	if ($text_left) {
		$leftClasses = ' col-md-5 col-xl-4 offset-xl-2';
		$rightClasses = ' col-md-6 col-xl-5';

		if (is_singular('post')) {
			$rightClasses = ' col-md-6 col-xl-4';
		}
	} else {
		$leftClasses = ' col-md-5 col-lg-4 offset-lg-1 text-lg-center';
		$rightClasses = ' col-md-6 col-lg-4';

		if (is_singular('post')) {
			$rightClasses = ' offset-md-1 col-md-6 col-lg-4 offset-lg-1';
		}
	}
?>
<section id="section<?= $i; ?>" class="section-columns" data-scroll-section>
	<div class="container-fluid">
		<div class="row paragraph paragraph-big">
			<?php if ($col0): ?>
				<div class="mb-10 mb-md-0<?= $leftClasses; ?>">
					<?= $col0; ?>
				</div>
			<?php endif; ?>
			<?php if ($col1): ?>
				<div class="<?= $rightClasses; ?>" data-scroll data-scroll-opacity>
					<?= $col1; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>