<?php
	$i = $args['index'];
	$title = get_sub_field('title');
	$text = get_sub_field('text');
	$img = get_sub_field('img');
	$img_big = get_sub_field('img_big');
	$btn = get_sub_field('ban');
?>
<section id="section<?= $i; ?>" class="section-columns section-lightbox pb-0" data-scroll-section>
	<div class="container-fluid pb-100">
		<div class="row paragraph paragraph-big">
			<div class="mb-10 mb-md-0 col-md-5 col-xl-4 offset-xl-2 mb-5 mb-md-0">
				<?php if ($title): ?>
				<h2 data-scroll data-splitting class="pb-100"><?= $title; ?></h2>
				<?php endif; ?>
				<a href="<?= $img_big['sizes']['full_hd']; ?>" data-id="lightbox<?= $i; ?>" class="hover-zoom js-lightbox">
					<span class="ratio ratio-1x1 d-inline-block" data-scroll data-scroll-opacity>
						<img src="<?= $img['sizes']['large']; ?>" alt="" class="img-fit w-100 h-100">
					</span>
				</a>
				<?php if ($btn): ?>
				<a href="<?= $img_big['sizes']['full_hd']; ?>" data-id="lightbox<?= $i; ?>" class="mt-sm-10 col-12 col-sm-10 js-lightbox hover-line"><?= $btn; ?>
					<svg enable-background="new 0 0 28.7 9.5" width="38" height="12" viewBox="0 0 28.7 9.5" xmlns="http://www.w3.org/2000/svg"><path d="m.8 4.8h27.2m-.1 0-4-4m4 4-4 4" fill="none" stroke="#1c5de0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
				</a>	
				<?php endif; ?>
			</div>
			<?php if ($text): ?>
				<div class="offset-md-1 col-md-6 col-xl-4" data-scroll data-scroll-opacity>
					<?= $text; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>


<div class="lightbox" data-id="lightbox<?= $i; ?>">
	<div class="lightbox-inner">
		<img src="<?= $img_big['sizes']['full_hd']; ?>" alt="">
	</div>
</div>