<?php
$i = $args['index'];
?>

<?php if (have_rows('slider')): ?>
	<section id="section<?= $i; ?>" class="section-slider overflow-hidden" data-scroll-section>
		<div class="container-fluid pb-100 mb-md-5">
			<div class="row">
				<div class="col-12 col-xl-10 offset-xl-2" data-scroll data-scroll-opacity>
					<div class="swiper-container slider-img" data-pagination="1" data-space="4">
						<div class="swiper-pagination pb-10 pt-5 d-none d-md-block"></div>
						<div class="swiper-wrapper">		
						<?php while( have_rows('slider') ) : the_row(); ?>
							<?php 
							$img = get_sub_field('img');
							$link = get_sub_field('link');
							if( $link ):
								$link_url = $link['url'];
								$link_title = $link['title'];
								$link_target = $link['target'] ? $link['target'] : '_self';
							?>
							<div class="swiper-slide hover-underline">
								<a href="<?= $link_url; ?>" class="hover-zoom<?php if ($link_target == '_self'): ?> js-custom-exit<?php endif; ?>">
									<figure class="overflow-hidden">
										<img src="<?= $img['url']; ?>" alt="<?= $img['alt']; ?>">
									</figure>
									<figcaption>
										<h3><?= $link_title; ?></h3>
										<span class="d-inline-block fs-14 mt-1 mt-sm-3 more"><?php _e('see more', 'nhc'); ?></span>
									</figcaption>
								</a>
							</div>
							<?php endif; ?>
						<?php endwhile; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
</section>
<?php endif; ?>