<?php
$i = $args['index'];
$img = get_sub_field('img');
$youtube = get_sub_field('yt');
$title = get_sub_field('title');
?>

<?php if ($img): ?>
	<section<?php if ($i): ?> id="section<?= $i; ?>"<?php endif; ?> class="section-img pt-10 mt-2" data-scroll-section>
		<?php if ($title): ?>
		<div class="container-fluid">
			<div class="row">
				<h2 class="col-12 col-xl-10 offset-xl-2 pb-100"><?= $title; ?></h2>
			</div>
		</div>
	<?php endif; ?>
	<?php if ($youtube): ?>
		<a href="#" class="js-lightbox position-relative d-block hover-zoom" data-id="lightbox<?= $i; ?>">
			<div data-scroll data-scroll-img data-scroll-speed="-.5">
				<img src="<?= $img['sizes']['full_hd']; ?>" alt="" class="w-100" data-scroll data-scroll-speed=".4">
				<img src="<?= get_template_directory_uri(); ?>/img/icons/yt.svg" alt="" class="youtube-play">
			</div>
		</a>
	<?php else: ?>
		<div data-scroll data-scroll-img data-scroll-speed="-.5">
			<img src="<?= $img['sizes']['full_hd']; ?>" alt="" class="w-100" data-scroll data-scroll-speed=".4">
		</div>
	<?php endif; ?>
</section>
<?php endif; ?>

<div class="lightbox" data-id="lightbox<?= $i; ?>">
	<div class="lightbox-inner">
		<div class="ratio ratio-16x9">
			<iframe width="560" height="315" src="<?= $youtube; ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>			
		</div>
	</div>
</div>