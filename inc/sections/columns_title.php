<?php
	$i = $args['index'];
	$title = get_sub_field('title');
	$col0 = get_sub_field('col0');
	$col1 = get_sub_field('col1');
?>
<section id="section<?= $i; ?>" class="section-columns pt-0" data-scroll-section>
	<div class="container-fluid pt-100">
		<div class="row paragraph paragraph-big">
			<h2 class="h2-medium col-12 col-xl-10 offset-xl-2" data-scroll data-splitting><?= $title; ?></h2>
			<?php if ($col0): ?>
				<div class="mb-10 mb-md-0 col-md-5 col-xl-4 offset-xl-2" data-scroll data-scroll-opacity>
					<?= $col0; ?>
				</div>
			<?php endif; ?>
			<?php if ($col1): ?>
				<div class="offset-md-1 col-md-6 col-xl-4" data-scroll data-scroll-opacity>
					<?= $col1; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>