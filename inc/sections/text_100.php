<?php
	$i = $args['index'];
	$title = get_sub_field('title');
	$bg_color = get_sub_field('bg_color');
	$shrink = get_sub_field('shrink');
	$text_color = get_sub_field('text_color');
	$text = get_sub_field('text');
	if ($shrink) {
		$class = ' col-12 col-md-8 col-lg-6 mx-auto px-xl-0';
	} else {
		$class = ' col-xl-8 offset-xl-2';
	}
?>
<section id="section<?= $i; ?>" class="section-text" data-scroll-section style="<?php if ($bg_color): ?>background-color: <?= $bg_color; ?>;<?php endif; ?><?php if ($text_color): ?>color: <?= $text_color; ?>;<?php endif; ?>">
	<div class="container-fluid">
		<div class="row">

			<?php if ($title): ?>
				<?php if ($i == 0): ?>
					<h1 class="col-xl-8 offset-xl-2 mb-5 mb-md-10" data-scroll data-splitting><?= $title; ?></h1>
				<?php else: ?>
					<div class="h1 col-xl-8 offset-xl-2 mb-5 mb-md-10" data-scroll data-splitting><?= $title; ?></div>
				<?php endif; ?>
			<?php endif; ?>

			<?php if ($text): ?>
				<div class="paragraph paragraph-big<?= $class; ?>" data-scroll data-scroll-opacity><?= $text; ?></div>
			<?php endif; ?>
		</div>
	</div>
</section>