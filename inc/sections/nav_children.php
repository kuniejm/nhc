<?php 
global $post;

if ( $post->post_parent ) {
  $parent = $post->post_parent;
} else {
  $parent = $post->ID;  
}
$args = array(
  'post_parent' => $parent, 
  'fields' => 'ids',
  'post_type'   => 'page',
  'orderby' => 'menu_order',
  'order' => 'ASC'
);
$children = get_children( $args);
?>
<?php if ( ! empty($children) ): ?> 
  <section class="section-nav overflow-hidden position-relative bg-white zIndex2"<?php if (wp_is_mobile()): ?> data-scroll-section<?php endif; ?>>
    <div class="container-fluid pe-10 pe-md-5 h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12 col-xl-10 offset-xl-2 h-100">
          <div class="swiper-container slider-nav h-100" data-space="90" data-loop="0">
            <ul class="swiper-wrapper h-100">
              <li class="swiper-slide"><a href="<?= get_permalink($parent); ?>" class="<?php if ($parent == $post->ID): ?>is-active <?php endif; ?>js-custom-exit">
                <?= get_the_title($parent); ?>
              </a></li>
              <?php foreach ($children as $key => $id): ?>
                <li class="swiper-slide"><a href="<?= get_permalink($id); ?>" class="<?php if ($id == $post->ID): ?>is-active <?php endif; ?>js-custom-exit">
                  <?= get_the_title($id); ?>
                </a></li>
              <?php endforeach; ?>
            </ul>
          </div>          
        </div>
      </div>
    </div>
    <div class="line-decorative is-active"></div>  
  </section>
  <?php endif; wp_reset_postdata(); ?>