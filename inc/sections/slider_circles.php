<?php
$i = $args['index'];
$title = get_sub_field('title');
$main_text = get_sub_field('text');
?>

<?php if (have_rows('slider')): ?>
	<section id="section<?= $i; ?>" class="section-circles overflow-hidden" data-scroll-section>
		<div class="container-fluid pt-100 pb-100 mb-md-10">
			<div class="row">
				<div class="col-md-7 pt-md-3 col-xl-5 offset-xl-2 pb-100">
					<h2 data-scroll data-splitting><?= $title; ?></h2>
				</div>
				<div class="col-12 col-md-5 pb-10" data-scroll data-scroll-opacity>
					<div class="d-flex my-3 my-md-0 justify-content-start">
						<a href="#" class="swiper-button swiper-button-prev">
							<svg enable-background="new 0 0 28.7 9.5" width="38" height="12" viewBox="0 0 28.7 9.5" xmlns="http://www.w3.org/2000/svg"><path d="m.8 4.8h27.2m-.1 0-4-4m4 4-4 4" fill="none" stroke="#1c5de0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
						</a>
						<a href="#" class="swiper-button swiper-button-next">
							<svg enable-background="new 0 0 28.7 9.5" width="38" height="12" viewBox="0 0 28.7 9.5" xmlns="http://www.w3.org/2000/svg"><path d="m.8 4.8h27.2m-.1 0-4-4m4 4-4 4" fill="none" stroke="#1c5de0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
						</a>
					</div>
				</div>
				<div class="col-12 slider-circles-container pb-100 mb-10" data-scroll data-scroll-opacity>
					<div class="swiper-container slider-circles" data-fade="1">
						<div class="swiper-wrapper">		
						<?php $i = 1; while( have_rows('slider') ) : the_row(); ?>
							<?php 
							$text = get_sub_field('text');
							$img = get_sub_field('img');
							$logo = get_sub_field('logo');
							?>
							<div class="swiper-slide">
								<div class="d-flex flex-column flex-md-row align-items-md-end">
									<div class="swiper-slide-img col-5 col-md-6 mb-10 mb-md-0">
										<div class="swiper-slide-img-circle"></div>
										<div class="ratio ratio-1x1">
											<img src="<?= $img['sizes']['large']; ?>" alt="<?= $img['alt']; ?>" class="img-fit w-100 h-100">
										</div>
									</div>
									<div class="swiper-slide-text col-12 col-sm-10 col-md-8">
										<div class="ratio ratio-1x1">
											<svg enable-background="new 0 0 78 78" width="339" height="339" fill="none" stroke="#040a5c" class="svg-circle" viewBox="0 0 78 78" xmlns="http://www.w3.org/2000/svg"><circle cx="39" cy="39" r="38" stroke-width=".2"/></svg>
										</div>
										<div class="paragraph hover-underline">
											<div class="swiper-slide-num mb-4 mb-lg-9" data-scroll data-splitting>
												<?php if ($j <= 9): ?>0<?php endif; ?><?= $i; ?>
											</div>												
											<?= $text; ?>
										</div>	

										<?php if ($logo): ?>
											<div class="img-logo ps-md-10 ms-md-5">
												<img src="<?= $logo['url']; ?>" alt="<?= $logo['alt']; ?>">
											</div>
										<?php endif; ?>										
									</div>
								</div>
							</div>
						<?php $i++; endwhile; ?>
						</div>
					</div>
				</div>
				<?php if ($main_text): ?>
				<div class="col-12 pt-100 mt-10 col-md-6 col-xl-4 offset-xl-7 paragraph hover-underline pe-md-10" data-scroll data-scroll-opacity>
					<?= $main_text; ?>
				</div>
				<?php endif; ?>
			</div>
		</div>
</section>
<?php endif; ?>