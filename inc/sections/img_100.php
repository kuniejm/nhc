<?php
$i = $args['index'];
$crop = $args['crop'];
$img = get_field('hero_img') ? get_field('hero_img') : get_sub_field('img');
$margin = get_sub_field('margin');
?>

<?php if ($img): ?>
	<section<?php if ($i): ?> id="section<?= $i; ?>"<?php endif; ?> class="section-img<?php if ($crop): ?> section-img-crop overflow-hidden<?php endif; ?>" data-scroll-section>
		<?php if ($margin): ?>
			<div class="container-fluid ps-0 ps-md-5 pe-0 overflow-hidden">
				<div class="row">
					<div class="col-12 col-xl-10 offset-xl-2">
						<div data-scroll data-scroll-img data-scroll-speed=".5">
							<img src="<?= $img['sizes']['full_hd']; ?>" alt="" class="w-100" data-scroll data-scroll-speed=".4">
						</div>
					</div>
				</div>
			</div>
		<?php else: ?>
			<div data-scroll data-scroll-speed="-3">
				<div class="position-relative zIndex2 overflow-hidden" data-scroll data-scroll-img>
					<div data-scroll data-scroll-speed="1.5">
						<img src="<?= $img['sizes']['full_hd']; ?>" alt="" class="w-100">	
					</div>	
				</div>
			</div>
	<?php endif; ?>
</section>
<?php endif; ?>