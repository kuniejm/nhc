<?php
	$i = $args['index'];
	$bg_color = get_sub_field('bg_color');
	$title = get_sub_field('title');
	$col0 = get_sub_field('col0');
	$img = get_sub_field('img');
?>
<section id="section<?= $i; ?>" class="section-columns py-0" data-scroll-section style="<?php if ($bg_color): ?>background-color: <?= $bg_color; ?>;<?php endif; ?>">
	<div class="container-fluid pt-100 pb-10 pb-md-0">
		<div class="row paragraph paragraph-big">
			<div class="paragraph col-12 col-xl-8 offset-xl-2" data-scroll data-splitting><?= str_replace('<h2', '<h2 class="h2-medium"', $title); ?>
				<p class="mb-0"></p>
			</div>
			<?php if ($col0): ?>
				<div class="col-md-5 col-xl-4 offset-xl-2 pb-100" data-scroll data-scroll-opacity>
					<?= $col0; ?>
				</div>
			<?php endif; ?>
			<?php if ($img): ?>
				<div class="section-columns-img col-md-7 col-xl-6" data-scroll data-scroll-opacity>
					<div class="col-10 col-sm-6 col-lg-10 mx-auto ms-md-auto mx-xl-auto position-relative">
						<div class="section-columns-img-circle"></div>		
						<div class="ratio ratio-1x1">
							<img src="<?= $img['sizes']['large']; ?>" alt="<?= $img['alt']; ?>" class="img-fit w-100 h-100">
						</div>						
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>