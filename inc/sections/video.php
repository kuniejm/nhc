<?php
$i = $args['index'];
$video = get_sub_field('video');
$icon = get_sub_field('icon');
$text = get_sub_field('text');
$crop = get_sub_field('crop');
$title = get_sub_field('title');
?>

<?php if ($video): ?>

	<?php if ($title): ?>
		<section data-scroll-section>
			<div class="container-fluid">
				<div class="row">
					<div class="col-12 offset-xl-2 col-md-8 py-10">
						<h2 class="words chars splitting is-inview"><?= $title; ?></h2>
					</div>	
				</div>
			</div>
		</section>
	<?php endif; ?>

	<section<?php if ($i): ?> id="section<?= $i; ?>"<?php endif; ?> class="section-video overflow-hidden<?php if ($crop): ?> section-video-nocrop<?php endif; ?>" data-scroll-section>
		<?php if ($icon): ?> 
			<div class="section-video-has-icon position-relative zIndex2" data-scroll data-scroll-opacity>
				<div class="section-video-icon">
					<img src="<?= get_template_directory_uri(); ?>/img/static/logo-signet.svg" alt="">
				</div>
			</div>
		<?php endif; ?>
		<div class="w-100 zIndex1" data-scroll data-scroll-img data-scroll-speed="-.5">
			<div class="w-100" >
				<video id="player<?= $i; ?>" class="player" autoplay controls="0" playsinline loop muted="1">
					<source src="<?= $video; ?>" type="video/mp4" class="w-100" data-scroll data-scroll-speed=".4" class="img-fit" />
				</video>
			</div>			
		</div>
		<?php if ($text): ?>
		<div class="container-fluid video-text my-10 my-sm-0<?php if ($icon): ?> video-text-margin<?php endif; ?>">
			<div class="row">
				<div class="<?php if (!$title): ?>col-12 offset-xl-2 col-md-8<?php endif; ?>">
					<?= $text; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</section>
<?php endif; ?>