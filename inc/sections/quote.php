<?php
	$i = $args['index'];
	$bg_color = get_sub_field('bg_color');
	$text_color = get_sub_field('text_color');
	$quote = get_sub_field('quote');
	$author = get_sub_field('author');
?>
<section id="section<?= $i; ?>" class="section-quote" data-scroll-section style="<?php if ($bg_color): ?>background-color: <?= $bg_color; ?>;<?php endif; ?><?php if ($text_color): ?>color: <?= $text_color; ?>;<?php endif; ?>">
	<div class="container-fluid">
		<div class="row">
			<blockquote class="col-xl-9 offset-xl-2">
				<p class="mark mb-0" data-scroll data-scroll-opacity>“</p>
				<span data-splitting data-scroll><?= $quote; ?></span>
				<?php if ($author): ?><p class="author mb-0" data-scroll data-scroll-opacity><?= $author; ?></p><?php endif; ?>
			</blockquote>
		</div>
	</div>
</section>