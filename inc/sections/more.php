<?php 
	$i = $args['index'];
	$type = get_sub_field('type');
	$title = get_sub_field($type . '_title');
?>
<section id="section<?= $i; ?>" class="section-more overflow-hidden" data-scroll-section>
	<div class="line-decorative" data-scroll></div>
	<div class="container-fluid<?php if ($type !== 'links'): ?> pe-0<?php endif; ?>">
		<div class="row">
			<div class="col-md-5 d-flex flex-column justify-content-between col-xl-3 offset-xl-2 pt-7<?php if ($type !== 'links'): ?> mb-5<?php endif; ?>">
				<h2 data-splitting data-scroll><?= $title; ?></h2>
				<?php if ($type == 'vertical'): ?>
					<div class="d-flex my-3 my-md-0 justify-content-start pb-150">
						<a href="#" class="swiper-button swiper-button-prev">
							<svg enable-background="new 0 0 28.7 9.5" width="38" height="12" viewBox="0 0 28.7 9.5" xmlns="http://www.w3.org/2000/svg"><path d="m.8 4.8h27.2m-.1 0-4-4m4 4-4 4" fill="none" stroke="#1c5de0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
						</a>
						<a href="#" class="swiper-button swiper-button-next">
							<svg enable-background="new 0 0 28.7 9.5" width="38" height="12" viewBox="0 0 28.7 9.5" xmlns="http://www.w3.org/2000/svg"><path d="m.8 4.8h27.2m-.1 0-4-4m4 4-4 4" fill="none" stroke="#1c5de0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
						</a>
					</div>
				<?php endif; ?>
			</div>
			<?php 
				if ($type == 'horizontal') {
					get_template_part('inc/sections/more-horizontal');
				} elseif ($type == 'links') {
					get_template_part('inc/sections/more-links');
				} else {
					get_template_part('inc/sections/more-vertical');
				}
			?>
		</div>
	</div>
</section>