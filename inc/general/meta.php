<?php 
  $desc = get_field('meta_desc', 'option');
  $img = get_field('meta_img', 'option');
?>  
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
 <?php if (is_singular('post') || is_singular('news')): ?>
   <meta property="og:title" content="<?= the_title_attribute(); ?>" />
    <meta property="og:description" content="<?= get_the_excerpt(); ?>" />
    <meta property="og:url" content="<?= get_permalink(); ?>" />
    <meta property="og:type" content="article" />
    <meta property="og:image" content="<?= get_the_post_thumbnail_url(); ?>" />
  <?php else: ?>
    <meta property="og:title" content="<?php if (!is_front_page()) {the_title_attribute(); echo ' - '; } ?><?= get_bloginfo( 'name' ); ?>" />
    <meta property="og:description" content="<?= $desc; ?>" />
    <meta property="og:url" content="<?= get_permalink(); ?>" />
    <meta property="og:image" content="<?= $img; ?>" />
  <?php endif; ?>