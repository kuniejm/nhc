<?php 
$linkedin = get_field('linkedin', 'options');
$twitter = get_field('twitter', 'options');
$facebook = get_field('facebook', 'options');
?>  
<ul class="socials d-flex align-items-xl-center justify-content-xl-end" data-scroll>

	<?php if ($linkedin): ?>
		<li>
			<a href="<?= $linkedin; ?>" target="_blank">
				<span class="circle d-inline-block">
					<svg version="1.1" width="42" height="42" viewBox="0 0 46 46" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px">
						<circle cx="23" cy="23" r="22.5" fill="none" stroke="#040a5c" stroke-width="1"></circle>
					</svg>
					<img src="<?= get_template_directory_uri(); ?>/img/icons/linkedin.svg" alt="">
				</span>
			</a>
		</li>
	<?php endif; ?>

	<?php if ($twitter): ?>
		<li>
			<a href="<?= $twitter; ?>" target="_blank">
				<span class="circle d-inline-block">
					<svg version="1.1" width="42" height="42" viewBox="0 0 46 46" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px">
						<circle cx="23" cy="23" r="22.5" fill="none" stroke="#040a5c" stroke-width="1"></circle>
					</svg>
					<img src="<?= get_template_directory_uri(); ?>/img/icons/twitter.svg" alt="">
				</span>
			</a>
		</li>
	<?php endif; ?>

	<?php if ($facebook): ?>
		<li>
			<a href="<?= $facebook; ?>" target="_blank">
				<span class="circle d-inline-block">
					<svg version="1.1" width="42" height="42" viewBox="0 0 46 46" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px">
						<circle cx="23" cy="23" r="22.5" fill="none" stroke="#040a5c" stroke-width="1"></circle>
					</svg>
					<img src="<?= get_template_directory_uri(); ?>/img/icons/facebook.svg" alt="">
				</span>
			</a>
		</li>
	<?php endif; ?>

</ul>